package org.eclipse.etrice.core.common.ide;

import org.eclipse.xtext.ide.server.symbol.DocumentSymbolMapper;
import org.eclipse.xtext.naming.QualifiedName;

public class SimpleSymbolNameProvider extends DocumentSymbolMapper.DocumentSymbolNameProvider {
	/**
	 * Returns the name of the symbol for the given object.
	 * This is the name that is displayed in the outline view in the editor and also in the document/workspace symbol search.
	 * For readability we use short (not fully qualified) names for the symbols.
	 * 
	 * @param object
	 * @return
	 */
	@Override
	public String getName(QualifiedName qualifiedName) {
		if (qualifiedName == null || qualifiedName.isEmpty()) { return null; }
		return qualifiedName.getLastSegment();
	}
}