/*******************************************************************************
 * Copyright (c) 2021 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.server;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.eclipse.etrice.generator.base.io.FileSystemModelPath.ModelDirectory;
import org.eclipse.etrice.core.common.ide.modelpath.ModelPathDescriptionLexer;
import org.eclipse.etrice.core.common.ide.modelpath.ModelPathDescriptionLexer.ModelPathDescriptionEntry;
import org.eclipse.etrice.generator.base.io.FileSystemModelPath;
import org.eclipse.etrice.generator.base.io.IModelPath;
import org.eclipse.lsp4j.FileChangeType;
import org.eclipse.lsp4j.FileEvent;
import org.eclipse.lsp4j.WorkspaceFolder;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.MultimapBuilder;
import com.google.inject.Inject;

import static org.eclipse.etrice.core.common.ide.modelpath.ModelPathDescriptionLexer.MODELPATH_FILE_NAME;

/**
 * Manages the modelpath configurations of all projects in the workspace.
 * The manager must be informed about changes to workspace folders as well as
 * changes to files in the workspace. 
 * 
 * Every folder with a modelpath file is considered an eTrice project with the name of the folder.
 */
public class ModelPathManager {
	private static final Logger LOG = Logger.getLogger(ModelPathManager.class);
	
	/** the max depth that the workspace folders are searched for modelpath files */
	private static final int MODELPATH_SEARCH_DEPTH = 8;
	
	/** the paths of all current workspace folders */
	private Set<Path> workspaceFolders;
	/** map from project path to its modelpath description */
	private Map<Path, ModelPathDescription> descriptions;
	/** map from project name to modelpath descriptions (project names are based on folder names and are thus *not* necessarily unique!) */
	private ListMultimap<String, ModelPathDescription> name2description;
	/** map from project path to modelpath for scope provider */
	private Map<Path, FileSystemModelPath> modelpaths;
	
	@Inject
	public ModelPathManager() {
		workspaceFolders = new LinkedHashSet<>();
		descriptions = new HashMap<>();
		name2description = MultimapBuilder.hashKeys().arrayListValues().build();
		modelpaths = Collections.emptyMap();
	}
	
	/**
	 * Retrieves the modelpath for the specified project.
	 * 
	 * @param uri the uri of the project
	 * @return the modelpath of the project
	 */
	public IModelPath getModelPath(String uri) {
		return uriToPath(uri).<IModelPath>map(modelpaths::get).orElse(IModelPath.EMPTY);
	}
	
	/**
	 * @param folders the workspace folders whose modelpath descriptions are to be returned
	 * @return all modelpath descriptions of projects within the specified folders
	 */
	public Stream<ModelPathDescription> getModelpathDescriptions(List<WorkspaceFolder> folders) {
		List<Path> folderPaths = folders.stream().flatMap(folder -> uriToPath(folder.getUri()).stream()).toList();
		return descriptions.values().stream()
			.filter(description -> folderPaths.stream().anyMatch(folderPath -> description.path.startsWith(folderPath)));
	}
	
	/**
	 * @param uri the uri of the project
	 * @return a set of all dependencies of the project including transitive dependencies
	 */
	public Set<String> getProjectDependencies(String uri) {
		return uriToPath(uri).map(descriptions::get)
			.<Set<String>>map(this::getAllDependencies).orElse(Collections.emptySet());
	}
	
	/**
	 * Informs this modelpath manager about added and removed workspace folders.
	 * 
	 * @param added the list of added workspace folders
	 * @param removed the list of removed workspace folders
	 */
	public void workspaceFoldersChanged(List<WorkspaceFolder> added, List<WorkspaceFolder> removed) {
		// remove the removed workspace folders
		List<Path> removedPaths = removed.stream().flatMap(folder -> uriToPath(folder.getUri()).stream()).toList();
		workspaceFolders.removeAll(removedPaths);
		
		// remove all projects that are not inside any of the remaining workspace folders (workspace folders are *not* necessarily disjoint!)
		List<Path> removedProjectPaths = descriptions.keySet().stream()
			.filter(path -> removedPaths.stream().anyMatch(folder -> path.startsWith(folder)))
			.filter(path -> !workspaceFolders.stream().anyMatch(folder -> path.startsWith(path)))
			.toList();
		removedProjectPaths.forEach(this::discardDescription);
		
		// add the added workspace folders
		List<Path> addedPaths = added.stream().flatMap(folder -> uriToPath(folder.getUri()).stream()).toList();
		workspaceFolders.addAll(addedPaths);
		
		// search for projects in the added workspace folders
		List<Path> addedProjectPaths = new ArrayList<>();
		addedPaths.forEach(workspaceFolder -> {
			try {
				Files.walkFileTree(workspaceFolder, Collections.emptySet(), MODELPATH_SEARCH_DEPTH, new SimpleFileVisitor<Path>() {
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						if(MODELPATH_FILE_NAME.equals(file.getFileName().toString()) && !descriptions.containsKey(file)) {
							addedProjectPaths.add(file.getParent());
						}
						return FileVisitResult.CONTINUE;
					}
				});
			}
			catch(IOException e) {
				LOG.error("Failed to traverse workspace folder " + workspaceFolder, e);
			}
		});
		// add new projects that don't exist yet (workspace folders are *not* necessarily disjoint!)
		addedProjectPaths.stream().filter(path -> !descriptions.containsKey(path)).forEach(this::loadDescription);
		
		computeModelPaths();
	}
	
	/**
	 * Informs this modelpath manager about file changes in the workspace.
	 * 
	 * @param changes the list of changes in the workspace
	 * @return {@code true} if the modelpath description of some projects changed and
	 * the workspace must be refreshed, {@code false} otherwise
	 */
	public boolean filesChanged(List<FileEvent> changes) {
		ArrayList<Path> dirtyDescriptions = new ArrayList<>();
		changes.forEach(event -> {
			uriToPath(event.getUri()).ifPresent(path -> {
				findDescription(path).ifPresent(description -> {
					// If the changed file is a modelpath description file, mark it as dirty.
					if(description.path.resolve(MODELPATH_FILE_NAME).equals(path)) {
						dirtyDescriptions.add(description.path);
					}
					if(event.getType() != FileChangeType.Changed) {
						// If the changed file belongs to a model directory, update the index of that directory.
						findDirectory(description, path).ifPresent(modelDir -> {
							if(event.getType() == FileChangeType.Created && Files.isRegularFile(path))
								modelDir.addFile(path);
							else if(event.getType() == FileChangeType.Deleted)
								modelDir.removeFile(path);
						});
					}
				});
			});
		});
		
		// Reload all changed modelpath description files and recompute the modelpaths if necessary.
		if(!dirtyDescriptions.isEmpty()) {
			dirtyDescriptions.forEach(path -> {
				discardDescription(path);
				loadDescription(path);
			});
			computeModelPaths();
			return true;
		}
		return false;
	}
	
	/**
	 * Computes the modelpaths of all projects currently in the workspace.
	 */
	private void computeModelPaths() {
		modelpaths = descriptions.values().stream().collect(Collectors.toMap(
			description -> description.path,
			description -> Stream
				.concat(Stream.of(description),
					getAllDependencies(description).stream().flatMap(name -> name2description.get(name).stream()))
				.flatMap(descr -> descr.modelDirs.stream())
				.collect(Collectors.collectingAndThen(Collectors.toList(), FileSystemModelPath::new))));
	}
	
	/**
	 * @param description the modelpath description of the project
	 * @return a set of all dependencies of the project including transitive depdendencies
	 */
	private HashSet<String> getAllDependencies(ModelPathDescription description) {
		HashSet<String> dependencies = new HashSet<>();
		description.dependencies.forEach(name -> addAllDependencies(name, dependencies));
		return dependencies;
	}
	
	// Recursive helper function to compute transitive dependencies
	private void addAllDependencies(String name, HashSet<String> dependencies) {
		if(dependencies.add(name)) {
			name2description.get(name).forEach(description ->
				description.dependencies.forEach(dependency ->
					addAllDependencies(dependency, dependencies)));
		}
	}
	
	/**
	 * Loads the modelpath description file of a project.
	 * 
	 * @param path the path to the project
	 */
	private void loadDescription(Path path) {
		// Parse the content of the modelpath description file
		Path file = path.resolve(MODELPATH_FILE_NAME);
		List<ModelPathDescriptionEntry> entries = Collections.emptyList();
		if(Files.isRegularFile(file)) {
			try(InputStream input = Files.newInputStream(file)) {
				entries = ModelPathDescriptionLexer.read(input); 
			}
			catch(IOException e) {
				LOG.error("Failed to read modelpath description file " + file, e);
			}
		}
		
		// Interpret the modelpath description entries
		List<ModelDirectory> modelDirs = new ArrayList<>();
		List<String> dependencies = new ArrayList<>();
		entries.forEach(entry -> {
			if(entry.key.equals(ModelPathDescriptionLexer.MODELPATH_KEYWORD_SRCDIR)) {
				try {
					Path dir = path.resolve(entry.value);
					modelDirs.add(new ModelDirectory(dir));
				}
				catch(InvalidPathException e) {
					LOG.error("Invalid model directory name " + entry.value + " in modelpath description " + file);
				}
			}
			else if(entry.key.equals(ModelPathDescriptionLexer.MODELPATH_KEYWORD_PROJECT)) {
				dependencies.add(entry.value);
			}
			else {
				LOG.error("Unknown keyword " + entry.key + " in modelpath description " + file);
			}
		});
		
		// Build the initial index of the model directories
		modelDirs.forEach(modelDir -> {
			if(Files.isDirectory(modelDir.getPath())) {
				try {
					modelDir.indexDirectory();
				}
				catch(IOException e) {
					LOG.error("Failed to read model directory " + modelDir.getPath() + " specified in modelpath description " + file, e);
				}
			}
			else {
				LOG.warn("Model directory " + modelDir.getPath() + " specified in modelpath description " + file + " does not exists.");
			}
		});
		
		// Update the maps with the new modelpath description
		ModelPathDescription description = new ModelPathDescription(path, modelDirs, dependencies);
		descriptions.put(path, description);
		name2description.put(path.getFileName().toString(), description);
	}
	
	/**
	 * Discards the modelpath description of a project.
	 * 
	 * @param path the path to the project
	 */
	private void discardDescription(Path path) {
		descriptions.remove(path);
		name2description.get(path.getFileName().toString()).removeIf(description -> description.path.equals(path));
	}
	
	/**
	 * @param path a path
	 * @return the modelpath description of the project that contains the path, if any
	 */
	private Optional<ModelPathDescription> findDescription(Path path) {
		return descriptions.values().stream()
			.filter(description -> path.startsWith(description.path))
			.max(Comparator.comparingInt(description -> description.path.getNameCount()));
	}
	
	/**
	 * @param description a modelpath description of a project
	 * @param path a path within the project
	 * @return the model directory that contains the path, if any
	 */
	private Optional<ModelDirectory> findDirectory(ModelPathDescription description, Path path) {
		Optional<ModelDirectory> result = description.modelDirs.stream()
			.filter(dir -> path.startsWith(dir.getPath()))
			.max(Comparator.comparingInt(dir -> dir.getPath().getNameCount()));
		return result;
	}
	
	/**
	 * Tries to convert a uri to a path.
	 * If this is not possible, an empty optional is returned and an error is logged. 
	 * 
	 * @param uri the uri to covert to a path
	 * @return maybe the converted path
	 */
	private static Optional<Path> uriToPath(String uri) {
		try {
			return Optional.of(Paths.get(java.net.URI.create(uri)));
		}
		catch(FileSystemNotFoundException e) {
			LOG.error("Unknown uri " + uri + "; " + e.getMessage(), e);
		}
		catch(IllegalArgumentException e) {
			LOG.error("Illegal uri " + uri + "; " + e.getMessage(), e);
		}
		return Optional.empty();
	}
	
	/** Represents a modelpath description file */
	public static class ModelPathDescription {
		/** path of the directory containing the modelpath file */
		public final Path path;
		/** model folders */
		public final List<ModelDirectory> modelDirs;
		/** project dependencies */
		public final List<String> dependencies;
		
		public ModelPathDescription(Path path, List<ModelDirectory> modelDirs, List<String> dependencies) {
			this.path = path;
			this.modelDirs = modelDirs;
			this.dependencies = dependencies;
		}
	}
	
}
