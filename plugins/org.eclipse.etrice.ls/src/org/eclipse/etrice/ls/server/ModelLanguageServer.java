/*******************************************************************************
 * Copyright (c) 2021 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.DidChangeWorkspaceFoldersParams;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.WorkspaceFolder;
import org.eclipse.lsp4j.WorkspaceFoldersChangeEvent;
import org.eclipse.sprotty.xtext.ls.SyncDiagramLanguageServer;
import org.eclipse.xtext.ide.server.BuildManager.Buildable;
import org.eclipse.xtext.util.CancelIndicator;

import com.google.inject.Inject;

/**
 * A language server implementation that extends the default implementation with
 * support for the modelpath import mechanism and modelpath description files.
 */
public class ModelLanguageServer extends SyncDiagramLanguageServer {
	
	@Inject
	private ModelPathManager modelpathManager;
	
	private List<WorkspaceFolder> workspaceFolders = new ArrayList<>();
	
	@Override
	public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
		if(getLanguageServerAccess().getInitializeParams() == null && params.getWorkspaceFolders() != null) {
			getRequestManager().runWrite(() -> {
				workspaceFolders.addAll(params.getWorkspaceFolders());
				// Inform the modelpath manager about the current workspace folders
				modelpathManager.workspaceFoldersChanged(params.getWorkspaceFolders(), Collections.emptyList());
				return null;
			}, (a, b) -> null);
		}
		return super.initialize(params);
	}
	
	@Override
	public void didChangeWorkspaceFolders(DidChangeWorkspaceFoldersParams params) {
		getRequestManager().runWrite(() -> {
			// Notify the modelpath manager about changed workspace folders
			modelpathManager.workspaceFoldersChanged(params.getEvent().getAdded(), params.getEvent().getRemoved());
			
			// If the workspace folders changed, refresh the whole workspace by first removing all current workspace folders and
			// then adding the updated list of all workspace folders again.
			// This is expensive but should make sure that no stale state remains because of changed project dependencies.
			getWorkspaceManager().didChangeWorkspaceFolders(new DidChangeWorkspaceFoldersParams(
					new WorkspaceFoldersChangeEvent(Collections.emptyList(), workspaceFolders)),
				CancelIndicator.NullImpl);
			workspaceFolders.removeAll(params.getEvent().getRemoved());
			workspaceFolders.addAll(params.getEvent().getAdded());
			getWorkspaceManager().didChangeWorkspaceFolders(new DidChangeWorkspaceFoldersParams(
					new WorkspaceFoldersChangeEvent(workspaceFolders, Collections.emptyList())),
					CancelIndicator.NullImpl);
			
			return null;
		}, (a, b) -> null);
	}
	
	@Override
	protected Buildable toBuildable(DidChangeWatchedFilesParams params) {
		// Inform the modelpath manager about changed files and refresh the workspace if the modelpath description of any project changed.
		boolean modelpathChanged = modelpathManager.filesChanged(params.getChanges());
		if(modelpathChanged) {
			// If any of the modelpath files changed, refresh the whole workspace by removing all workspace folders and then adding them again.
			// This is expensive but should make sure that no stale state remains because of changed project dependencies.
			getWorkspaceManager().didChangeWorkspaceFolders(new DidChangeWorkspaceFoldersParams(
					new WorkspaceFoldersChangeEvent(Collections.emptyList(), workspaceFolders)), CancelIndicator.NullImpl);
			getWorkspaceManager().didChangeWorkspaceFolders(new DidChangeWorkspaceFoldersParams(
					new WorkspaceFoldersChangeEvent(workspaceFolders, Collections.emptyList())), CancelIndicator.NullImpl);
		}
		
		// Notify the super class about the changes
		return super.toBuildable(params);
	}
}
