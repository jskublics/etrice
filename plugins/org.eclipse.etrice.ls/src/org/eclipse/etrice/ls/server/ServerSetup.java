/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.server;

import com.google.inject.Module;
import org.eclipse.elk.alg.layered.options.LayeredMetaDataProvider;
import org.eclipse.elk.core.util.persistence.ElkGraphResourceFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.sprotty.layout.ElkLayoutEngine;
import org.eclipse.sprotty.xtext.launch.DiagramLanguageServerSetup;

public class ServerSetup extends DiagramLanguageServerSetup {

	@Override
	public void setupLanguages() {
		ElkLayoutEngine.initialize(new LayeredMetaDataProvider());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("elkg", new ElkGraphResourceFactory());
	}
	
	@Override
	public Module getLanguageServerModule() {
		return new ServerModule();
	}

}
