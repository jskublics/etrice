/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Julian Skublics
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.EnumSet;

import org.eclipse.elk.alg.layered.options.LayeredOptions;
import org.eclipse.elk.core.math.ElkMargin;
import org.eclipse.elk.core.math.ElkPadding;
import org.eclipse.elk.core.math.KVector;
import org.eclipse.elk.core.options.Alignment;
import org.eclipse.elk.core.options.CoreOptions;
import org.eclipse.elk.core.options.Direction;
import org.eclipse.elk.core.options.EdgeLabelPlacement;
import org.eclipse.elk.core.options.EdgeRouting;
import org.eclipse.elk.core.options.EdgeType;
import org.eclipse.elk.core.options.HierarchyHandling;
import org.eclipse.elk.core.options.NodeLabelPlacement;
import org.eclipse.elk.core.options.PortAlignment;
import org.eclipse.elk.core.options.PortConstraints;
import org.eclipse.elk.core.options.PortLabelPlacement;
import org.eclipse.elk.core.options.SizeConstraint;
import org.eclipse.sprotty.Action;
import org.eclipse.sprotty.SGraph;
import org.eclipse.sprotty.SModelRoot;
import org.eclipse.sprotty.layout.ElkLayoutEngine;
import org.eclipse.sprotty.layout.SprottyLayoutConfigurator;

public class LayoutEngine extends ElkLayoutEngine {

	@Override
	public void layout(SModelRoot root, Action cause) {
		if (root instanceof SGraph) {
			//StructureDiagram Layout
			SprottyLayoutConfigurator configurator = new SprottyLayoutConfigurator();
			configurator.configureByType(DiagramGenerator.STRUCTURE_DIAGRAM_TYPE)
				.setProperty(CoreOptions.DIRECTION, Direction.DOWN);
			configurator.configureByType(DiagramGenerator.ERROR_MESSAGE_TYPE)
				.setProperty(CoreOptions.ALIGNMENT, Alignment.CENTER);
			configurator.configureByType(StructureDiagramGenerator.ACTOR_TYPE)
				.setProperty(CoreOptions.DIRECTION, Direction.UP)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.OUTSIDE, NodeLabelPlacement.V_BOTTOM, NodeLabelPlacement.H_LEFT))
				.setProperty(CoreOptions.NODE_SIZE_CONSTRAINTS, EnumSet.of(SizeConstraint.MINIMUM_SIZE, SizeConstraint.NODE_LABELS, SizeConstraint.PORTS, SizeConstraint.PORT_LABELS))
				.setProperty(CoreOptions.NODE_SIZE_MINIMUM, new KVector(200, 125))
				.setProperty(CoreOptions.PORT_CONSTRAINTS, PortConstraints.FREE)
				.setProperty(CoreOptions.PORT_ALIGNMENT_DEFAULT, PortAlignment.DISTRIBUTED)
				.setProperty(CoreOptions.PORT_LABELS_PLACEMENT, EnumSet.of(PortLabelPlacement.OUTSIDE))
				.setProperty(CoreOptions.EDGE_ROUTING, EdgeRouting.POLYLINE)
				.setProperty(CoreOptions.MARGINS, new ElkMargin(StructureDiagramGenerator.PORT_SIZE))
				.setProperty(CoreOptions.PADDING, new ElkPadding(StructureDiagramGenerator.PORT_SIZE));
			configurator.configureByType(StructureDiagramGenerator.INTERFACE_PORT_TYPE)
				.setProperty((CoreOptions.PORT_BORDER_OFFSET), -StructureDiagramGenerator.PORT_SIZE / 2);
			configurator.configureByType(StructureDiagramGenerator.INTERNAL_PORT_TYPE)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.OUTSIDE, NodeLabelPlacement.V_BOTTOM, NodeLabelPlacement.H_CENTER));
			configurator.configureByType(StructureDiagramGenerator.SPP_EXTERNAL_TYPE)
				.setProperty((CoreOptions.PORT_BORDER_OFFSET), -StructureDiagramGenerator.PORT_SIZE / 2);
			configurator.configureByType(StructureDiagramGenerator.SPP_REF_TYPE)
				.setProperty((CoreOptions.PORT_BORDER_OFFSET), -StructureDiagramGenerator.REF_PORT_SIZE / 2);
			configurator.configureByType(StructureDiagramGenerator.ACTOR_REF_TYPE)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.INSIDE, NodeLabelPlacement.V_CENTER, NodeLabelPlacement.H_CENTER))
				.setProperty(CoreOptions.NODE_SIZE_CONSTRAINTS, EnumSet.of(SizeConstraint.MINIMUM_SIZE, SizeConstraint.NODE_LABELS, SizeConstraint.PORTS, SizeConstraint.PORT_LABELS))
				.setProperty(CoreOptions.NODE_SIZE_MINIMUM, new KVector(100, 60))
				.setProperty(CoreOptions.PORT_CONSTRAINTS, PortConstraints.FREE)
				.setProperty(CoreOptions.PORT_ALIGNMENT_DEFAULT, PortAlignment.DISTRIBUTED)
				.setProperty(CoreOptions.PORT_LABELS_PLACEMENT, EnumSet.of(PortLabelPlacement.INSIDE))
				.setProperty(CoreOptions.MARGINS, new ElkMargin(StructureDiagramGenerator.REF_PORT_SIZE))
				.setProperty(CoreOptions.PADDING, new ElkPadding(StructureDiagramGenerator.REF_PORT_SIZE));
			configurator.configureByType(StructureDiagramGenerator.SUBSTRUCTURE_INDICATOR_TYPE)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.INSIDE, NodeLabelPlacement.V_BOTTOM, NodeLabelPlacement.H_RIGHT))
				.setProperty(CoreOptions.NODE_LABELS_PADDING, new ElkPadding(10));
			configurator.configureByType(StructureDiagramGenerator.REF_PORT_TYPE)
				.setProperty((CoreOptions.PORT_BORDER_OFFSET), -StructureDiagramGenerator.REF_PORT_SIZE / 2);
			
			//BehaviorDiagram Layout
			configurator.configureByType(DiagramGenerator.BEHAVIOR_DIAGRAM_TYPE)
				.setProperty(CoreOptions.ALGORITHM, LayeredOptions.ALGORITHM_ID)
				.setProperty(CoreOptions.DIRECTION, Direction.RIGHT);
			configurator.configureByType(BehaviorDiagramGenerator.STATE_MACHINE_TYPE)
				.setProperty(CoreOptions.ALGORITHM, LayeredOptions.ALGORITHM_ID)
				.setProperty(CoreOptions.HIERARCHY_HANDLING, HierarchyHandling.INCLUDE_CHILDREN)
				.setProperty(CoreOptions.DIRECTION, Direction.UNDEFINED)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.OUTSIDE, NodeLabelPlacement.V_BOTTOM, NodeLabelPlacement.H_LEFT))
				.setProperty(CoreOptions.NODE_SIZE_CONSTRAINTS, EnumSet.of(SizeConstraint.MINIMUM_SIZE, SizeConstraint.NODE_LABELS, SizeConstraint.PORTS, SizeConstraint.PORT_LABELS))
				.setProperty(CoreOptions.NODE_SIZE_MINIMUM, new KVector(200, 125))
				.setProperty(CoreOptions.EDGE_ROUTING, EdgeRouting.SPLINES);
			configurator.configureByType(BehaviorDiagramGenerator.STATE_TYPE)
				.setProperty(CoreOptions.ALGORITHM, LayeredOptions.ALGORITHM_ID)
				.setProperty(CoreOptions.EDGE_ROUTING, EdgeRouting.SPLINES)
				.setProperty(CoreOptions.HIERARCHY_HANDLING, HierarchyHandling.SEPARATE_CHILDREN)
				.setProperty(CoreOptions.PORT_CONSTRAINTS, PortConstraints.FREE)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.INSIDE, NodeLabelPlacement.V_CENTER, NodeLabelPlacement.H_CENTER))
				.setProperty(CoreOptions.NODE_SIZE_CONSTRAINTS, EnumSet.of(SizeConstraint.MINIMUM_SIZE, SizeConstraint.NODE_LABELS, SizeConstraint.PORTS, SizeConstraint.PORT_LABELS))
				.setProperty(CoreOptions.EDGE_TYPE, EdgeType.DIRECTED);
			configurator.configureByType(BehaviorDiagramGenerator.SUBGRAPH_INDICATOR)
				.setProperty(CoreOptions.NODE_LABELS_PLACEMENT, EnumSet.of(NodeLabelPlacement.INSIDE, NodeLabelPlacement.V_BOTTOM, NodeLabelPlacement.H_RIGHT))
				.setProperty(CoreOptions.NODE_LABELS_PADDING, new ElkPadding(10));
			configurator.configureByType(BehaviorDiagramGenerator.INIT_POINT_TYPE)
				.setProperty(CoreOptions.ALIGNMENT, Alignment.TOP);
			configurator.configureByType(BehaviorDiagramGenerator.TRANSITION_TYPE)
				.setProperty(CoreOptions.SPACING_EDGE_LABEL, 2.0)
				.setProperty(CoreOptions.SPACING_LABEL_NODE, 10.0);
			configurator.configureByType(BehaviorDiagramGenerator.EDGE_LABEL)
				.setProperty(CoreOptions.EDGE_LABELS_PLACEMENT, EdgeLabelPlacement.CENTER);
			configurator.configureByType(BehaviorDiagramGenerator.TRANSITIONPOINT_TYPE)
				.setProperty(CoreOptions.PORT_BORDER_OFFSET, -BehaviorDiagramGenerator.OUTER_TRANSITIONPOINT_SIZE / 2);
			configurator.configureByType(BehaviorDiagramGenerator.EXITPOINT_TYPE)
				.setProperty((CoreOptions.PORT_BORDER_OFFSET), -BehaviorDiagramGenerator.INNER_TRANSITIONPOINT_SIZE / 2);
			configurator.configureByType(BehaviorDiagramGenerator.ENTRYPOINT_TYPE)
				.setProperty((CoreOptions.PORT_BORDER_OFFSET), -BehaviorDiagramGenerator.INNER_TRANSITIONPOINT_SIZE / 2);
			layout((SGraph) root, configurator, cause);
		}
	}
	
}
