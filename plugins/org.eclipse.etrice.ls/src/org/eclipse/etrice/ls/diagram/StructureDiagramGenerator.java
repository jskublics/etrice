/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Julian Skublics
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.Collections;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.etrice.core.fsm.fSM.FSMPackage;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorContainerRef;
import org.eclipse.etrice.core.room.Binding;
import org.eclipse.etrice.core.room.BindingEndPoint;
import org.eclipse.etrice.core.room.Port;
import org.eclipse.etrice.core.room.RoomElement;
import org.eclipse.etrice.core.room.RefSAPoint;
import org.eclipse.etrice.core.room.RelaySAPoint;
import org.eclipse.etrice.core.room.SPP;
import org.eclipse.etrice.core.room.util.RoomHelpers;
import org.eclipse.sprotty.Dimension;
import org.eclipse.sprotty.SEdge;
import org.eclipse.sprotty.SLabel;
import org.eclipse.sprotty.SModelElement;
import org.eclipse.sprotty.SNode;
import org.eclipse.sprotty.xtext.IDiagramGenerator.Context;
import org.eclipse.sprotty.xtext.SIssueMarkerDecorator;
import org.eclipse.sprotty.xtext.tracing.ITraceProvider;

public class StructureDiagramGenerator {
	
	private static final RoomHelpers roomHelper = new RoomHelpers();

	// TODO move sizing to layout engine
	public static final double PORT_SIZE = 40d;
	public static final double REF_PORT_SIZE = 18d;
	public static final double SP_SIZE = 18d;
	
	public static final String ACTOR_TYPE = "structure:actor";
	public static final String ACTOR_REF_TYPE = "structure:actorRef";
	public static final String SUBSTRUCTURE_INDICATOR_TYPE = "structure:subStructureIndicator";
	public static final String INTERFACE_PORT_TYPE = "structure:interfacePort";
	public static final String INTERNAL_PORT_TYPE = "structure:internalPort";
	public static final String REF_PORT_TYPE = "structure:portRef";
	public static final String BINDING_TYPE = "structure:bindingEdge";
	public static final String LAYERING_TYPE = "structure:layeringEdge";
	public static final String SPP_EXTERNAL_TYPE = "structure:spp";
	public static final String SPP_REF_TYPE = "structure:spp_ref";

	
	private ITraceProvider traceProvider;
	private SIssueMarkerDecorator issueMarker;

	public StructureDiagramGenerator(ITraceProvider traceProvider, SIssueMarkerDecorator issueMarker) {
		this.traceProvider = traceProvider;
		this.issueMarker = issueMarker;
	}


	public SNode generateStructureDiagram(ActorClass ac, Context context) {

		SLabel actorName = new SLabel(label -> {
			label.setId(uniqueActorId(ac, context, "#label"));
			label.setText(ac.getName());
		});
		
		Stream<SServiceProvisionPoint> serviceProvisionPoints = includingInherited(ac, ActorClass::getServiceProvisionPoints).map(p -> {
			SLabel portName = new SLabel(label -> {
				label.setId(uniqueActorId(ac, context, toId(p) + "#label"));
				label.setText(p.getName());
				traceProvider.trace(label, p, FSMPackage.Literals.ABSTRACT_INTERFACE_ITEM__NAME, -1);
			});
			return new SServiceProvisionPoint(port -> {
				port.setId(uniqueActorId(ac, context, toId(p)));
				port.setSize(new Dimension(PORT_SIZE, PORT_SIZE));
				port.setChildren(Collections.singletonList(portName));
				traceAndMark(port, p, context);
			});
		});

		Stream<SInterfacePort> interfacePorts = includingInherited(ac, ActorClass::getInterfacePorts).map(p -> {
			SLabel portName = new SLabel(label -> {
				label.setId(uniqueActorId(ac, context, toId(p) + "#label"));
				label.setText(p.getName());
			});
			return new SInterfacePort(port -> {
				port.setIsConjugated(p.isConjugated());
				port.setMultiplicity(p.getMultiplicity());
				port.setIsRelayPort(roomHelper.isRelay(p));
				port.setId(uniqueActorId(ac, context, toId(p)));
				port.setChildren(Collections.singletonList(portName));
				port.setSize(new Dimension(PORT_SIZE, PORT_SIZE));
				traceAndMark(port, p, context);
			});
		});
		Stream<SInternalPort> internalPorts = includingInherited(ac, ActorClass::getInternalPorts).map(p -> {
			SLabel portName = new SLabel(label -> {
				label.setId(uniqueActorId(ac, context, toId(p) + "#label"));
				label.setText(p.getName());
			});
			return new SInternalPort(node -> {
				node.setIsConjugated(p.isConjugated());
				node.setMultiplicity(p.getMultiplicity());
				node.setIsRelayPort(roomHelper.isRelay(p));
				node.setId(uniqueActorId(ac, context, toId(p)));
				node.setChildren(Collections.singletonList(portName));
				node.setSize(new Dimension(PORT_SIZE, PORT_SIZE));
				traceAndMark(node, p, context);
			});
		});
		
		Stream<SActorRef> actorRefs = includingInherited(ac, ActorClass::getActorRefs).map(ref -> {
			SLabel refName = new SLabel(label -> {
				label.setId(uniqueActorId(ac, context, toId(ref) + "#label"));
				label.setText(ref.getName());
			});
			Stream<SInterfacePort> refPorts = includingInherited(ref.getType(), ActorClass::getInterfacePorts).map(p -> {
				SLabel portName = new SLabel(label -> {
					label.setId(uniqueActorId(ac, context, toId(ref, p) + "#label"));
					label.setText(p.getName());
				});
				return new SInterfacePort(port -> {
					port.setType(REF_PORT_TYPE);
					port.setIsConjugated(p.isConjugated());
					port.setMultiplicity(p.getMultiplicity());
					port.setIsRelayPort(roomHelper.isRelay(p));
					port.setId(uniqueActorId(ac, context, toId(ref, p)));
					port.setChildren(Collections.singletonList(portName));
					port.setSize(new Dimension(REF_PORT_SIZE, REF_PORT_SIZE));
					traceAndMark(port, p, context);
				});
			});
			Stream<SServiceProvisionPoint> spps = includingInherited(ref.getType(), ActorClass::getServiceProvisionPoints).map(p -> {
				SLabel portName = new SLabel(label -> {
					label.setId(uniqueActorId(ac, context, toId(ref,p) + "#label"));
					label.setText(p.getName());
					traceProvider.trace(label, p, FSMPackage.Literals.ABSTRACT_INTERFACE_ITEM__NAME, -1);
				});
				return new SServiceProvisionPoint(port -> {
					port.setType(SPP_REF_TYPE);
					port.setId(uniqueActorId(ac, context, toId(ref,p)));
					port.setSize(new Dimension(SP_SIZE, SP_SIZE));
					port.setChildren(Collections.singletonList(portName));
					traceAndMark(port, p, context);
				});
			});
			List<SModelElement> children = Stream.of(Stream.of(refName),refPorts,spps).flatMap(s->s).collect(Collectors.toList());
			if (!ref.getType().getActorRefs().isEmpty()) {
				children.add(new SLabel(indicator -> {
					indicator.setType(SUBSTRUCTURE_INDICATOR_TYPE);
					indicator.setId(uniqueActorId(ac, context, toId(ref) + "#subStructure"));
					//This Text is not rendered in sprotty, but the layout engine needs a placeholder to compute the position of the label
					indicator.setText("placeholder");
				}));
			}
			return new SActorRef(node -> {
				node.setType(ACTOR_REF_TYPE);
				node.setId(uniqueActorId(ac, context, toId(ref)));
				node.setChildren(children);
				node.setFileUri(ref.getType().eResource().getURI().toString());
				node.setActorName(ref.getType().getName());
				traceAndMark(node, ref, context);
			});
		});
		
		Stream<SEdge> bindings = includingInherited(ac, ActorClass::getBindings).map(b -> {
			String endpoint1 = withActorId(ac, toId(b.getEndpoint1()));
			String endpoint2 = ac.getName() + "$" + toId(b.getEndpoint2());
			return new SEdge(edge -> {
				edge.setType(BINDING_TYPE);	
				edge.setId(uniqueActorId(ac, context, toId(b)));
				edge.setSourceId(endpoint1);
				edge.setTargetId(endpoint2);
				traceAndMark(edge, b, context);
			});
		});
		
		Stream<SEdge> layerConnections = includingInherited(ac, ActorClass::getConnections).map(c -> {		
			
			String endpoint1 = withActorId(ac, c.getFrom() instanceof RefSAPoint ?
					toId(((RefSAPoint)c.getFrom()).getRef())
					: toId(((RelaySAPoint) c.getFrom()).getRelay()));
			String endpoint2 = withActorId(ac, toId(c.getTo().getRef(), c.getTo().getService()));
			return new SEdge(edge -> {
				edge.setType(LAYERING_TYPE);	
				edge.setId(uniqueActorId(ac, context, endpoint1 + "->" + endpoint2));
				edge.setSourceId(endpoint1);
				edge.setTargetId(endpoint2);
				traceAndMark(edge, c, context);
			});
		});
		
		List<SModelElement> children = Stream.of(Stream.of(actorName) ,serviceProvisionPoints, layerConnections , interfacePorts, internalPorts, actorRefs, bindings)
			.<SModelElement>flatMap(Function.identity()).collect(Collectors.toList());
		return new SNode(node -> {
			node.setType(ACTOR_TYPE);
			node.setId(context.getIdCache().uniqueId(ac, ac.getName()));
			node.setChildren(children);
			traceAndMark(node, ac, context);
		});
	}
	
	private String toId(SPP p) {
		return p.getName();
	}
	private String toId(Binding b) {
		return toId(b.getEndpoint1()) + "<->" + toId(b.getEndpoint2());
	}
	
	private String toId(BindingEndPoint ep) {
		return toId(ep.getActorRef(), ep.getPort());
	}
	
	private String toId(ActorContainerRef ref, Port p) {
		return ref != null ? toId(ref) + "." + toId(p) : toId(p);
	}
	
	private String toId(ActorContainerRef ref, SPP p) {
		return ref != null ? toId(ref) + "." + toId(p) : toId(p);
	}
	
	private String toId(ActorContainerRef ref) {
		return ref.getName();
	}
	
	private String toId(Port p) {
		return p.getName();
	}
	
	private String withActorId(ActorClass ac, String id) {
		return ac.getName() + "$" + id;
	}
	
	private String uniqueActorId(ActorClass ac, Context context, String idProposal) {
		return context.getIdCache().uniqueId(withActorId(ac, idProposal));
	}

	private void traceAndMark(SModelElement sElement, RoomElement element, Context context) {
		traceProvider.trace(sElement, element);
		issueMarker.addIssueMarkers(sElement, element, context);
	}


	
	private static <T> Stream<T> includingInherited(ActorClass ac, Function<ActorClass, List<T>> getter) {
		return Stream.<ActorClass>iterate(ac, Objects::nonNull, ActorClass::getActorBase).flatMap(cls -> getter.apply(cls).stream());
	}
}
