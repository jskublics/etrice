/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.function.Consumer;

import org.eclipse.sprotty.SNode;

/**
 * Sprotty node for displaying standalone error messages.
 * This node type is used to show an error message in the diagram view in cases where
 * no diagram can be shown for some reason (e.g. if the cursor is not positioned in an actor class).
 */
public class SErrorMessage extends SNode {
	private String errorMessage;
	
	public SErrorMessage() {
		this.setType(DiagramGenerator.ERROR_MESSAGE_TYPE);
	}
	public SErrorMessage(final Consumer<SErrorMessage> initializer) {
		this();
		initializer.accept(this);
	}
	public String getErrorMessage() {
		return this.errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
