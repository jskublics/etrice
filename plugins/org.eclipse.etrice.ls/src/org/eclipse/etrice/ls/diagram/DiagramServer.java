/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;


import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.sprotty.RequestModelAction;
import org.eclipse.sprotty.xtext.LanguageAwareDiagramServer;

public class DiagramServer extends LanguageAwareDiagramServer {

	@Override
	protected void handle(RequestModelAction request) {	
		
		URI sourceUri = URI.create(request.getOptions().get("sourceUri"));		
		request.getOptions().putAll(splitQuery(sourceUri));
					
		
		/*
		 * The implementation of the LanguageAwareDiagramServer requires a URI without a query String, so a new URI with query = null is created
		 * If no authority is specified java.net.URI removes the "//" between scheme and path of the URI 
		 * To compensate this, an empty string is provided if no authority is specified in the original URI
		 */
		try {
			URI newUri = new URI(sourceUri.getScheme(), sourceUri.getAuthority() == null ? "" : sourceUri.getAuthority(), sourceUri.getPath(), null, sourceUri.getFragment());
			request.getOptions().put("sourceUri", newUri.toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		super.handle(request);
	}

	/*
	 * Generates a KeyValue-Map based on the query parameters of a URI
	 */
	private Map<String,String> splitQuery(URI uri) {
		String query = uri.getQuery();
		if (query == null) {
			return Collections.emptyMap();
		}
		return Arrays.stream(query.split("&"))
				.map(string -> string.split("="))
				.filter(arr -> arr.length == 2)
				.collect(Collectors.toMap(arr -> arr[0], arr -> arr[1], (existing, replacement) -> replacement));
	}
	
	
	/*
	 * getOptions() is saved across multiple RequestModelActions.
	 * The KeyValue Pairs in getOptions() are only overwritten if the Key is present in the new Options Map.
	 * To prevent unintentional behavior all custom options are deleted after each Graph generation.
	 */
	@Override
	protected void copyOptions(RequestModelAction request) {
		getOptions().remove("actorName");
		getOptions().remove("cursorOffset");
		super.copyOptions(request);
	}

}
