package org.eclipse.etrice.ls.diagram;


import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.fsm.fSM.impl.ChoicePointImpl;
import org.eclipse.etrice.core.fsm.fSM.impl.EntryPointImpl;
import org.eclipse.etrice.core.fsm.fSM.impl.ExitPointImpl;
import org.eclipse.etrice.core.fsm.fSM.impl.SimpleStateImpl;
import org.eclipse.etrice.core.fsm.fSM.impl.TransitionPointImpl;
import org.eclipse.etrice.core.room.impl.ActorRefImpl;
import org.eclipse.etrice.core.room.impl.BindingImpl;
import org.eclipse.etrice.core.room.impl.LayerConnectionImpl;
import org.eclipse.etrice.core.room.impl.PortImpl;
import org.eclipse.etrice.core.room.impl.SPPImpl;
import org.eclipse.sprotty.HtmlRoot;
import org.eclipse.sprotty.IDiagramServer;
import org.eclipse.sprotty.PreRenderedElement;
import org.eclipse.sprotty.RequestPopupModelAction;
import org.eclipse.sprotty.SIssueMarker;
import org.eclipse.sprotty.SModelElement;
import org.eclipse.sprotty.SModelRoot;
import org.eclipse.sprotty.xtext.ILanguageAwareDiagramServer;
import org.eclipse.sprotty.xtext.PopupModelFactory;
import org.eclipse.sprotty.xtext.tracing.ITraceProvider;

import com.google.inject.Inject;

public class EtricePopupModelFactory extends PopupModelFactory {
	
	private static final Logger LOG = Logger.getLogger(EtricePopupModelFactory.class);
	
	@Inject
	private ITraceProvider traceProvider;
	
	@Override
	public SModelRoot createPopupModel(SModelElement element, RequestPopupModelAction request, IDiagramServer server) {
		String diagramType = server.getOptions().get("diagramType");
		if (element instanceof SIssueMarker) {
			return super.createPopupModel(element, request, server);
		}
		return new HtmlRoot(root -> {
			root.setId(element.getId() + "#popup");
			root.setCanvasBounds(request.getBounds());
			root.setCssClasses(List.of("sprotty-popup-container"));
			try {
				root.setChildren(Collections.singletonList(
					traceProvider.withSource(element, (ILanguageAwareDiagramServer)server, (eObject,context) -> {
						if (diagramType.equals("behavior-diagram")) {
							return createBehaviorPopup(eObject, element);
						} else if (diagramType.equals("structure-diagram")) {
							return createStructurePopup(eObject,element);
						}
						throw new IllegalArgumentException("DiagramType " + diagramType + " is not implemented by the PopupModelFactory");
					}).get()
				));
			} catch (ExecutionException | InterruptedException e) {
				LOG.error(e);
			}
		});
	}
	
	private PreRenderedElement createBehaviorPopup(EObject eObject, SModelElement element) {
		if (eObject instanceof SimpleStateImpl state) {
			String name = state.getName();
			return createSimplePopupText(element, "State: " + name);
			
		} else if (eObject instanceof ChoicePointImpl choicePoint) {
			String name = choicePoint.getName();
			return createSimplePopupText(element, "Choice Point: " + name);
			
		} else if (eObject instanceof EntryPointImpl entryPoint) {
			String name = entryPoint.getName();
			return createSimplePopupText(element, "Entry Point: " + name);
			
		} else if (eObject instanceof ExitPointImpl exitPoint) {
			String name = exitPoint.getName();
			return createSimplePopupText(element, "Exit Point: " + name);
			
		} else if (eObject instanceof TransitionPointImpl transitionPoint) {
			String name = transitionPoint.getName();
			return createSimplePopupText(element, "Transition Point: " + name);
			
		} else {
			/* 
			 * This exception can be caused by SModelElements with an activated PopupFeature without a popup implementation for their corresponding eObject.
			 * Either add a popup implementation here or disable the popupFeature for the concerning SModelElement
			 * in the corresponding di.config.ts file located at vscode-extenxion/webview/src/behavior.
			 */
			throw new IllegalArgumentException("PopupModelFactory is not implemented for the eObject Subclass " + eObject.getClass());
		}
	}
	
	private PreRenderedElement createStructurePopup(EObject eObject, SModelElement element) {
		if (eObject instanceof PortImpl port) {
			String name = port.getName();
			String protocol = port.getProtocol().getName();
			if (port.isConjugated()) {
				protocol = "conj " + protocol;
			}
			return createSimplePopupText(element, name + " (" + protocol + ")");
			
		} else if (eObject instanceof ActorRefImpl actorRef){
			String name = actorRef.getName();
			String actorClass = actorRef.getType().getName();
			return createSimplePopupText(element, name + " (" + actorClass + ")");
			
		} else if (eObject instanceof BindingImpl binding){
			String from = binding.getEndpoint1().getActorRef().getName() + "." + binding.getEndpoint1().getPort().getName();
			String to = binding.getEndpoint2().getActorRef().getName() + "." + binding.getEndpoint2().getPort().getName();
			String protocol = binding.getEndpoint1().getPort().getProtocol().getName();
			return createSimplePopupText(element,"Binding " + from + " and " + to + " (" + protocol + ")");
			
		} else if (eObject instanceof SPPImpl spp){
			String name = spp.getName();
			String protocol = spp.getProtocol().getName();
			return createSimplePopupText(element, name + " (" + protocol + ")");
	
		} else if (eObject instanceof LayerConnectionImpl layerConnection) {
			String service = layerConnection.getTo().getService().getName();
			return createSimplePopupText(element, service);
			
		} else {
			/* 
			 * This exception can be caused by SModelElements with an activated PopupFeature without a popup implementation for their corresponding eObject.
			 * Either add a popup implementation here or disable the popupFeature for the concerning SModelElement
			 * in the corresponding di.config.ts file located at vscode-extenxion/webview/src/structure.
			 */
			throw new IllegalArgumentException("PopupModelFactory is not implemented for the eObject Subclass " + eObject.getClass());
		}
	}
	
	private static PreRenderedElement createSimplePopupText(SModelElement element, String text) {
		return new PreRenderedElement(popup -> {
			popup.setType("pre-rendered");
			popup.setId(element.getId() + "#code");
			popup.setCode("<div class=\"sprotty-popup-text\">" + text + "</div>");
		});
	}
}
