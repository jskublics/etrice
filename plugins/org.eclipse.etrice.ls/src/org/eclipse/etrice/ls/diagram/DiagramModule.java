/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Julian Skublics
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;


import org.eclipse.etrice.core.ide.IRoomDiagramModule;
import org.eclipse.sprotty.IDiagramServer;
import org.eclipse.sprotty.ILayoutEngine;
import org.eclipse.sprotty.IPopupModelFactory;
import org.eclipse.sprotty.xtext.DefaultDiagramModule;
import org.eclipse.sprotty.xtext.IDiagramGenerator;
import org.eclipse.sprotty.xtext.IDiagramServerFactory;

public class DiagramModule extends DefaultDiagramModule implements IRoomDiagramModule {
	//Service Provider for the IRoomDiagramModule
	@Override
	public Class<? extends IDiagramServerFactory> bindIDiagramServerFactory() {
		return EtriceDiagramServerFactory.class;
	}
	
	public Class<? extends IDiagramGenerator> bindIDiagramGenerator() {
		return DiagramGenerator.class;
	} 
	
	public Class<? extends ILayoutEngine> bindILayoutEngine() {
		return LayoutEngine.class;
	}
	
	@Override
	public Class<? extends IDiagramServer> bindIDiagramServer() {
		return DiagramServer.class;
	}
	
	@Override
	public Class<? extends IPopupModelFactory> bindIPopupModelFactory() {
		return EtricePopupModelFactory.class;
	}
	

}
