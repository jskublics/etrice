/*******************************************************************************
 * Copyright (c) 2012 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.structure.support.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorContainerRef;
import org.eclipse.etrice.core.room.Binding;
import org.eclipse.etrice.core.room.InterfaceItem;
import org.eclipse.etrice.core.room.LayerConnection;
import org.eclipse.etrice.core.room.Port;
import org.eclipse.etrice.core.room.StructureClass;
import org.eclipse.etrice.ui.structure.DiagramAccess;
import org.eclipse.etrice.ui.structure.support.DiagramUtil;
import org.eclipse.etrice.ui.structure.support.Pos;
import org.eclipse.etrice.ui.structure.support.PosAndSize;
import org.eclipse.etrice.ui.structure.support.DiagramUtil.Border;
import org.eclipse.etrice.ui.structure.support.SupportUtil;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.ILinkService;

public class SuperDiagramPositionProvider implements IPositionProvider {

	private class Parent {
		String key;
		PosAndSize posInv;
	}

	private class ElementPosition {
		final PosAndSize pos;
		final Border border;
		
		ElementPosition(PosAndSize pos, Border border) {
			this.pos = pos;
			this.border = border;
		}
	}
	
	private PosAndSize posInvSuper;
	private HashMap<String, ElementPosition> obj2pos;
	private HashMap<String, Pos> obj2text;
	private HashMap<String, ArrayList<Pos>> obj2bendpoints;
	private Parent parent;

	private SuperDiagramPositionProvider(){	}
	
	public SuperDiagramPositionProvider(StructureClass sc) {
		obj2pos = new HashMap<String, ElementPosition>();
		obj2text = new HashMap<String, Pos>();
		obj2bendpoints = new HashMap<String, ArrayList<Pos>>();
		
		// defaults
		parent = new Parent();
		parent.key = getKey(sc);
		// parent.posInv is initialized in mapPositions()
		
		if (sc instanceof ActorClass) {
			ActorClass base = ((ActorClass) sc).getActorBase();
			if (base != null)
				mapPositions(base);
		}
	}

	@Override
	public SuperDiagramPositionProvider setNewParent(EObject parent, PosAndSize invisibleRect, PosAndSize innerRect) {
		SuperDiagramPositionProvider pp = new SuperDiagramPositionProvider();
		pp.obj2pos = this.obj2pos;
		pp.obj2text = this.obj2text;
		pp.obj2bendpoints = this.obj2bendpoints;
		pp.posInvSuper = this.posInvSuper;
		
		Parent newParent = new Parent();
		newParent.key = getKey(parent);
		newParent.posInv = invisibleRect;
		
		pp.parent = newParent;
		
		return pp;
	}

	@Override
	public boolean contains(EObject obj) {
		String key = getKey(obj);
		return obj2pos.containsKey(key) || obj2text.containsKey(key) || obj2bendpoints.containsKey(key);
	}

	@Override
	public PosAndSize getDiagramPosition() {
		return posInvSuper;
	}

	@Override
	public PosAndSize getPosition(EObject bo) {
		ElementPosition elem = obj2pos.get(getKey(bo));
		if (elem == null)
			return null;

		int x = elem.pos.getX();
		int y = elem.pos.getY();
		int w = elem.pos.getW();
		int h = elem.pos.getH();
		switch (elem.border) {
		case BOTTOM:
			if (parent.posInv.getH() > posInvSuper.getH())
				y = elem.pos.getY() + (parent.posInv.getH() - posInvSuper.getH());
			break;
		case RIGHT:
			if (parent.posInv.getW() > posInvSuper.getW())
				x = elem.pos.getX()+(parent.posInv.getW() - posInvSuper.getW());
			break;
		case NONE:
		case TOP:
		case LEFT:
		default:
			break;
		}
		return new PosAndSize(x,y,w,h);
	}

	@Override
	public Pos getConnectionText(EObject obj) {
		Pos p = obj2text.get(getKey(obj));
		int offsetX = parent.posInv.getX() - posInvSuper.getX();
		int offsetY = parent.posInv.getY() - posInvSuper.getY();
		return new Pos((p.getX() + offsetX), (p.getY() + offsetY));
	}

	@Override
	public List<Pos> getConnectionBendpoints(EObject obj) {
		ArrayList<Pos> list = obj2bendpoints.get(getKey(obj));
		int offsetX = parent.posInv.getX() - posInvSuper.getX();
		int offsetY = parent.posInv.getY() - posInvSuper.getY();
		return (list == null || list.isEmpty()) ? Collections.emptyList()
				: new ArrayList<Pos>(list).stream()
						.map(p -> new Pos((p.getX() + offsetX), (p.getY() + offsetY)))
						.collect(Collectors.toList());
	}

	private void mapPositions(ActorClass ac) {
		Diagram diagram = new DiagramAccess().getDiagram(ac);
		if (diagram == null)
			return;

		ContainerShape acShape = DiagramUtil.findScShape(diagram);
		if(acShape == null)
			return;
		
		ILinkService linkService = Graphiti.getLinkService();
		EObject sc = linkService.getBusinessObjectForLinkedPictogramElement(acShape);
		if(!sc.eResource().getURI().equals(ac.eResource().getURI()))
			return;

		posInvSuper = DiagramUtil.getPosAndSize(acShape.getGraphicsAlgorithm());
		parent.posInv = posInvSuper;

		// refs & interface items
		for (Shape shape : acShape.getChildren()) {
			EObject obj = linkService.getBusinessObjectForLinkedPictogramElement(shape);
			
			// positions
			if (obj instanceof ActorContainerRef || obj instanceof InterfaceItem) {
				
				PosAndSize objInv = DiagramUtil.getPosAndSize(shape.getGraphicsAlgorithm());
				Border border = Border.NONE;
				// refs interface items
				if (obj instanceof ActorContainerRef){
					for(Shape child : ((ContainerShape)shape).getChildren()){
						EObject childBo = linkService.getBusinessObjectForLinkedPictogramElement(child);
						if(!(childBo instanceof InterfaceItem))
							continue;
						PosAndSize ifInv = DiagramUtil.getPosAndSize(child.getGraphicsAlgorithm());
						obj2pos.put(getKey(obj)+getKey(childBo), new ElementPosition(ifInv, Border.NONE));
					}
				} else if (obj instanceof InterfaceItem) {
					if (obj instanceof Port) {
						Port p = (Port)obj;
						if (!SupportUtil.getInstance().getRoomHelpers().isInternal(p)) {
							border = DiagramUtil.getNearestBorder(objInv, posInvSuper);
						}
					}
				}
				obj2pos.put(getKey(obj), new ElementPosition(objInv, border));
			}
		}

		// bindings & layer connections
		for (Connection conn : diagram.getConnections()) {
			EObject obj = linkService.getBusinessObjectForLinkedPictogramElement(conn);

			if (obj instanceof Binding || obj instanceof LayerConnection) {

				// label position
				if (conn.getConnectionDecorators().size() > 1) {
					ConnectionDecorator cd = conn.getConnectionDecorators().get(1);
					if (cd.getGraphicsAlgorithm() instanceof Text) {
						GraphicsAlgorithm ga = cd.getGraphicsAlgorithm();
						Pos pos = new Pos(ga.getX(), ga.getY());
						obj2text.put(getKey(obj), pos);
					}
				}
				
				// bend points
				if (conn instanceof FreeFormConnection) {
					ArrayList<Pos> points = new ArrayList<Pos>();
					for (Point bp : ((FreeFormConnection) conn).getBendpoints()) {
						Pos pos = new Pos(bp.getX(), bp.getY());
						points.add(pos);
					}
					if(!points.isEmpty())
						obj2bendpoints.put(getKey(obj), points);
				}
			}
		}
		
	}

	private String getKey(EObject bo){
		// short path
		if(bo instanceof StructureClass)
			return "";
		
		return parent.key+DiagramUtil.getResourcePath(bo);
	}
}
