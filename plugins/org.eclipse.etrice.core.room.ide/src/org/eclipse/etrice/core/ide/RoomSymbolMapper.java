package org.eclipse.etrice.core.ide;

import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.common.base.BasePackage;
import org.eclipse.etrice.core.common.ide.PatchedDocumentSymbolService;
import org.eclipse.etrice.core.fsm.fSM.ChoicepointTerminal;
import org.eclipse.etrice.core.fsm.fSM.FSMPackage;
import org.eclipse.etrice.core.fsm.fSM.InitialTransition;
import org.eclipse.etrice.core.fsm.fSM.NonInitialTransition;
import org.eclipse.etrice.core.fsm.fSM.StateTerminal;
import org.eclipse.etrice.core.fsm.fSM.SubStateTrPointTerminal;
import org.eclipse.etrice.core.fsm.fSM.TrPointTerminal;
import org.eclipse.etrice.core.fsm.fSM.TransitionTerminal;
import org.eclipse.etrice.core.room.Attribute;
import org.eclipse.etrice.core.room.Message;
import org.eclipse.etrice.core.room.Operation;
import org.eclipse.etrice.core.room.RefableType;
import org.eclipse.etrice.core.room.RoomPackage;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.xtext.ide.server.symbol.DocumentSymbolMapper;
import org.eclipse.xtext.ide.server.symbol.HierarchicalDocumentSymbolService;

/**
 * This class maps eTrice room model elements to document/workspace symbols.
 * It is used in the {@link HierarchicalDocumentSymbolService} to provide document symbols as well as in the {@link PatchedDocumentSymbolService} for workspace symbols.
 * <br>
 * We consider all elements that have an explicit name in the model as symbols.
 */
public class RoomSymbolMapper {
	
	public static class RoomSymbolKindProvider extends DocumentSymbolMapper.DocumentSymbolKindProvider {
		/**
		 * Returns the symbol kind for the given class. 
		 * Afaik the symbol kind is only used to determine the icon in the outline view in the editor (and in symbol searches).
		 * The symbol kinds chosen below are purely chosen based on aesthetics of the icons.
		 * See the following blog post for an overview of the icons displayed in VSCode: https://adamcoster.com/blog/vscode-workspace-symbol-provider-purpose#example-workspacesymbolprovider-implementation
		 * <br>
		 * <b>NOTE</b>: Symbols are only shown if this method does <i>not</i> return {@code null} as the symbol kind. 
		 * @param object
		 * @return
		 */
		@Override
		protected SymbolKind getSymbolKind(EClass clazz) {
			if(clazz == null) { return null; }
			if(clazz.getEPackage() == RoomPackage.eINSTANCE) {    			
				switch(clazz.getClassifierID()) {
					// room model content
					case RoomPackage.PRIMITIVE_TYPE, RoomPackage.EXTERNAL_TYPE:
						return SymbolKind.TypeParameter;
					case RoomPackage.ENUMERATION_TYPE: return SymbolKind.Enum;
					case RoomPackage.DATA_CLASS: return SymbolKind.Struct;
					case RoomPackage.PROTOCOL_CLASS: return SymbolKind.Interface;
					case RoomPackage.ACTOR_CLASS, RoomPackage.SUB_SYSTEM_CLASS, RoomPackage.LOGICAL_SYSTEM:
						return SymbolKind.Class;
					
					// room class content
					case RoomPackage.ATTRIBUTE: return SymbolKind.Field;
					case RoomPackage.CLASS_STRUCTOR: return SymbolKind.Constructor;
					case RoomPackage.STANDARD_OPERATION: return SymbolKind.Method;
					// enum content
					case RoomPackage.ENUM_LITERAL: return SymbolKind.EnumMember;
					// protocol class content
					case RoomPackage.MESSAGE: return SymbolKind.Event;
					case RoomPackage.PORT_OPERATION: return SymbolKind.Method;
					// actor container class content
					case RoomPackage.PORT, RoomPackage.SAP, RoomPackage.SPP:
						return SymbolKind.Interface;
					case RoomPackage.ACTOR_REF: return SymbolKind.Class;
					// logical system content
					case RoomPackage.LOGICAL_THREAD: return SymbolKind.Object;
				}
			} else if(clazz.getEPackage() == FSMPackage.eINSTANCE) {
				switch(clazz.getClassifierID()) {
					// fsm content
					case FSMPackage.SIMPLE_STATE: return SymbolKind.Constant;
					case FSMPackage.TRANSITION_POINT: return SymbolKind.Number;
					case FSMPackage.ENTRY_POINT, FSMPackage.EXIT_POINT:
						return SymbolKind.Interface;
					case FSMPackage.CHOICE_POINT: return SymbolKind.Operator;
					case FSMPackage.INITIAL_TRANSITION, FSMPackage.TRIGGERED_TRANSITION, FSMPackage.GUARDED_TRANSITION,
						FSMPackage.CP_BRANCH_TRANSITION, FSMPackage.CONTINUATION_TRANSITION:
						return SymbolKind.Boolean;
				}
			} else if(clazz.getEPackage() == BasePackage.eINSTANCE) {
				switch(clazz.getClassifierID()) {
					case BasePackage.ANNOTATION_TYPE: return SymbolKind.String;
				}
			}
			return null;
		}
	}
	
	public static class RoomSymbolDetailsProvider extends DocumentSymbolMapper.DocumentSymbolDetailsProvider {
		/**
		 * Returns the details for the given eobject which is shown next to the name of the symbol in the outline view.
		 * <br>
		 * <b>CAUTION</b>: This method must be able to handle broken/incomplete models, i.e. expect {@code null}s everywhere in the model!
		 *  
		 * @param object
		 * @return 
		 */
		@Override
		public String getDetails(EObject object) {
			if (object instanceof Attribute attr) {
				return getRefTypeName(attr.getType());
			} else if (object instanceof Operation operation) {
				String params = operation.getArguments().stream()
					.map(param -> getRefTypeName(param.getRefType()))
					.collect(Collectors.joining(", ", "(", ")"));
				String returnType = operation.getReturnType() != null ? getRefTypeName(operation.getReturnType()) : "void";
				return params + " -> " + returnType;
			} else if (object instanceof Message message) {
				String payloadType = message.getData() != null ? getRefTypeName(message.getData().getRefType()) : "void";
				boolean isIncoming = message.eContainmentFeature().getFeatureID() == RoomPackage.PROTOCOL_CLASS__INCOMING_MESSAGES;
				return payloadType + (isIncoming ? " (incoming)" : " (outgoing)");
			} else if (object instanceof InitialTransition tr) {
				String to = getTransitionTerminalName(tr.getTo());
				return "initial -> " + to;
			} else if (object instanceof NonInitialTransition tr) {
				String from = getTransitionTerminalName(tr.getFrom());
				String to = getTransitionTerminalName(tr.getTo());
				return from + " -> " + to;
			}
			return "";
		}
		
		private String getTransitionTerminalName(TransitionTerminal terminal) {
			if (terminal instanceof ChoicepointTerminal choicepointTerminal
					&& choicepointTerminal.getCp() != null && choicepointTerminal.getCp().getName() != null) {
				return choicepointTerminal.getCp().getName();
			} else if (terminal instanceof StateTerminal stateTerminal
					&& stateTerminal.getState() != null && stateTerminal.getState().getName() != null) {
				return stateTerminal.getState().getName();
			} else if (terminal instanceof TrPointTerminal trPointTerminal
					&& trPointTerminal.getTrPoint() != null && trPointTerminal.getTrPoint().getName() != null) {
				return "my." + trPointTerminal.getTrPoint().getName();
			} else if (terminal instanceof SubStateTrPointTerminal subStateTerminal
					&& subStateTerminal.getState() != null && subStateTerminal.getState().getName() != null
					&& subStateTerminal.getTrPoint() != null && subStateTerminal.getTrPoint().getName() != null) {
				return subStateTerminal.getState().getName() + "." + subStateTerminal.getTrPoint().getName();
			}
			return "?"; // incomplete/broken model
		}
		
		private String getRefTypeName(RefableType refType) {
			if(refType == null || refType.getType() == null || refType.getType().getName() == null) {
				return "?"; // incomplete/broken model
			}
			return refType.getType().getName() + (refType.isRef() ? " ref" : "");
		}
	}
}
