/*******************************************************************************
 * Copyright (c) 2011 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.common.ui.hover.highlight;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

/**
 * Hover help highlighter
 */
public class XtextHtmlHighlighter {

	public static final String HTML_CLASS_HIGHLIGHTEDFLAG = "customHighlighted";

	private XtextTokenScanner scanner;

	public XtextHtmlHighlighter(XtextTokenScanner scanner) {
		this.scanner = scanner;
	}

	public String htmlStartTags() {
		return "<pre><code class=" + scanner.getLanguageName() + " " + HTML_CLASS_HIGHLIGHTEDFLAG + ">";
	}

	public String highlight(String text) {
		StringBuilder builder = new StringBuilder(text);

		IDocument document = new Document(text);
		scanner.setRange(document, 0, document.getLength());
		
		IToken next;
		int pointer = 0;
		while ((next = scanner.nextToken()) != Token.EOF) {
			if (next instanceof XtextTokenScanner.CodeToken) {
				String pre = "<span class=" + next.getData() + ">";
				String post = "</span>";
				builder.insert(pointer + scanner.getTokenLength(), post);
				builder.insert(pointer, pre);
				pointer += pre.length() + post.length();
			}
			pointer += scanner.getTokenLength();
		}

		return builder.toString();
	}

	public String htmlEndTags() {
		return "</code></pre>";
	}
}
