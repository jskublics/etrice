/*******************************************************************************
 * Copyright (c) 2011 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.common.ui.hover;

public interface IKeywordHoverContentProvider {

	/**
	 * Returns CommonMark snippet for given keyword
	 */
	default String getMdContent(String name) {
		return null;
	}
	
	/** Migrate to {@link IKeywordHoverContentProvider#getMdContent(String)} */
	@Deprecated
	default String getHTMLContent(String name) {
		return null;
	}
}

