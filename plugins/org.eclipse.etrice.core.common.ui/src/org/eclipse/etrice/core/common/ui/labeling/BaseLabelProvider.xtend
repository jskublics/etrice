/*******************************************************************************
 * Copyright (c) 2013 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Eyrak Paen (initial contribution)
 * 
 *******************************************************************************/
package org.eclipse.etrice.core.common.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.etrice.core.common.base.Import
import org.eclipse.jface.resource.JFaceResources
import org.eclipse.jface.viewers.StyledString
import org.eclipse.jface.viewers.StyledString.Styler
import org.eclipse.swt.graphics.RGB
import org.eclipse.swt.graphics.TextStyle
import org.eclipse.xtext.ui.label.DefaultEObjectLabelProvider

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class BaseLabelProvider extends DefaultEObjectLabelProvider {

	protected static final String KEYWORD_COLOR = "KEYWORD_COLOR";

	protected val fontRegistry = JFaceResources.fontRegistry	// Bug 576866 
	protected val colorRegistry = JFaceResources.getColorRegistry

	val keyWordStyler = new Styler() {

		override applyStyles(TextStyle textStyle) {
			textStyle.font = fontRegistry.getBold(JFaceResources.TEXT_FONT)
			textStyle.foreground = colorRegistry.get(KEYWORD_COLOR)
		}

	}

	val typeStyler = new Styler() {

		override applyStyles(TextStyle textStyle) {
			textStyle.font = fontRegistry.getItalic(JFaceResources.TEXT_FONT)
		}

	}

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);

		colorRegistry.put(KEYWORD_COLOR, new RGB(50, 50, 50));
	}

	def String image(Import mdl) {
		return null;
	}

	def StyledString text(Import im) {
		if (im.getImportedNamespace() === null) {
			var txt = new StyledString("import model " + im.getImportURI())
			txt.setStyle(0, 12, getKeywordStyler())
			return txt
		} else {
			var txt = new StyledString("import ns " + im.getImportedNamespace())
			txt.setStyle(0, 9, getKeywordStyler())
			return txt
		}
	}
	
	def protected Styler getKeywordStyler() {
		return keyWordStyler
	}

	def protected Styler getTypeStyler() {
		return typeStyler
	}

}
