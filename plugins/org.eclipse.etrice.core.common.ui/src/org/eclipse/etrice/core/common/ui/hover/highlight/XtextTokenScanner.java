/*******************************************************************************
 * Copyright (c) 2011 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.common.ui.hover.highlight;

import java.util.List;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;

import com.google.common.collect.Lists;

/**
 * Generic token scanner for hover help highlighting
 */
public class XtextTokenScanner extends RuleBasedScanner {

	protected String languageName;
	
	protected final IToken keywordToken = new CodeToken("keyword");
	protected final IToken stringToken = new CodeToken("string");
	protected final IToken commentToken = new CodeToken("comment");

	// remember last token: avoid keyword within ids
	private char cachedToken = (char) UNDEFINED;
	
	protected WordRule keywordRule = new WordRule(new IWordDetector() {

		@Override
		public boolean isWordStart(char c) {
			return !Character.isWhitespace(c);
		}

		@Override
		public boolean isWordPart(char c) {
			return Character.isJavaIdentifierPart(c);
		}

	}, Token.UNDEFINED, false);
	
	protected boolean isId(char c){
		return Character.isLetter(c) || Character.isDigit(c) || c == '_';
	}

	public XtextTokenScanner(String languageName, String[] keywords) {
		this.languageName = languageName;

		setRules(computeRules(keywords).toArray(new IRule[0]));
	}

	public String getLanguageName() {
		return languageName;
	}

	protected List<IRule> computeRules(String[] keywords) {
		List<IRule> rules = Lists.newArrayList();

		rules.add(new EndOfLineRule("//", commentToken));
		rules.add(new MultiLineRule("/*", "*/", commentToken));
		rules.add(new MultiLineRule("'''", "'''", stringToken));
		
		rules.add(new SingleLineRule("\"", "\"", stringToken, '\\'));
		rules.add(new SingleLineRule("'", "'", stringToken, '\\'));

		// Add rule for language keywords.
		for (int i = 0; i < keywords.length; i++)
			keywordRule.addWord(keywords[i], keywordToken);
		rules.add(keywordRule);

		return rules;
	}
	
	@Override
	public IToken nextToken() {
		char last = cachedToken;
		IToken result = super.nextToken();
		
		unread();
		char end = (char) read();
				
		char next = cachedToken = (char) read();
		if(result == keywordToken && (isId(next) || isId(last)))
			return fDefaultReturnToken;
		
		unread();
		cachedToken = end;
		
		return result;
	}
	
	public static class CodeToken extends Token {

		public CodeToken(String name) {
			super(name);
		}
		
	}
}
