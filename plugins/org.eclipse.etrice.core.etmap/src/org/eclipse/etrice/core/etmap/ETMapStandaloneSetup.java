/*******************************************************************************
 * Copyright (c) 2012 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Henrik Rentz-Reichert (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.etmap;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.etrice.core.etmap.eTMap.ETMapPackage;

import com.google.inject.Injector;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class ETMapStandaloneSetup extends ETMapStandaloneSetupGenerated{

	public static void doSetup() {
		new ETMapStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
	@Override
	public void register(Injector injector) {
		if (!EPackage.Registry.INSTANCE.containsKey(ETMapPackage.eNS_URI)) {
			EPackage.Registry.INSTANCE.put(ETMapPackage.eNS_URI, ETMapPackage.eINSTANCE);
		}
		super.register(injector);
	}
}

