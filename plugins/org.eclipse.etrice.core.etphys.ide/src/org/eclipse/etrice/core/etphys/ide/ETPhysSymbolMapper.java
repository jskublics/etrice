package org.eclipse.etrice.core.etphys.ide;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.etrice.core.etphys.eTPhys.ETPhysPackage;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.xtext.ide.server.symbol.DocumentSymbolMapper;

/** See RoomSymbolMapper */
public class ETPhysSymbolMapper {
	public static class ETPhysSymbolKindProvider extends DocumentSymbolMapper.DocumentSymbolKindProvider {
		@Override
		protected SymbolKind getSymbolKind(EClass clazz) {
			if(clazz == null) { return null; }
			if(clazz.getEPackage() == ETPhysPackage.eINSTANCE) {    			
				switch(clazz.getClassifierID()) {
					// physical model content
					case ETPhysPackage.PHYSICAL_SYSTEM: return SymbolKind.Struct;
					case ETPhysPackage.NODE_CLASS: return SymbolKind.Class;
					case ETPhysPackage.RUNTIME_CLASS: return SymbolKind.Enum;
					
					// physical system content
					case ETPhysPackage.NODE_REF: return SymbolKind.Field;
					// node class content
					case ETPhysPackage.PHYSICAL_THREAD: return SymbolKind.Object;
				}
			}
			return null;
		}
	}
}
