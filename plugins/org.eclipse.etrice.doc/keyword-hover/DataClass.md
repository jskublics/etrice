A DataClass is a composition of Attributes

```room
/** Documentation */
DataClass DataClassName {
	Attribute ... // nested DataClasses possible
	ctor ''' /* constructor */ '''
	dtor ''' /* destructor */ '''
	Operation ...
}

DataClass DataClassName extends DataSuperClassName {
	// inherits all elements from super class
	...
}
```