A LayerConnection associates a SPP to an ActorRef, resulting in an connection of all SAPs on its instance hierarchy

```room
LayerConnection ref actorRef1 satisfied_by actorRef2.spp2
LayerConnection relay_sap spp1 satisfied_by actorRef2.spp2
```