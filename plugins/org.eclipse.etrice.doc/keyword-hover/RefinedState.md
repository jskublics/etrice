A RefinedState refines a State of one of the Actor's base class state machines

```room
RefinedState BaseState {
	exit '''// this is derived exit code'''
	subgraph {
		// add a sub state machine
		Transition init: initial -> state0 { }
		State state0
	}
}
```