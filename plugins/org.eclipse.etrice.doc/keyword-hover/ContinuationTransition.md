the continuation transition is a transition with just an optional action code

```room
State running {
	subgraph {
		EntryPoint ep0
		State active
		Transition tr0: my ep0 -> active
	}
}
```