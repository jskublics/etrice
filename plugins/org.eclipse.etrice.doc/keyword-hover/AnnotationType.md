AnnotationTypes can be used to tag ROOM classes for further custom processing

```room
/** documentation */
AnnotationType AnnotationName {
	target = ActorClass
	mandatory attribute name: ptCharacter
	optional attribute name: ptInteger
}
```