A PrimitiveType is an abstraction of a target language's basic type (e.g. integer or boolean)

```room
// Java
PrimitiveType boolean: ptBoolean -> boolean (Boolean) default "false"
PrimitiveType int16: ptInteger -> short (Short) default "0"

// c
PrimitiveType boolean: ptBoolean -> boolean default "false"
PrimitiveType int16: ptInteger -> int16 default "0"

/** Documentation */
PrimitiveType name: literalType -> targetName (castName) default "value"
```