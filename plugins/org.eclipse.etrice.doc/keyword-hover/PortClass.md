A PortClass allows a specifc implementation of a port class of a protocol

```room
// This example uses the port class interface provided by the c code generator
ProtocolClass PingPong {
	incoming {
		Message ping(uint8)
	}
	outgoing {
		Message pong(uint8)
	}
	regular PortClass {
		//...
	}
	conjugated PortClass {
		// ...
	}
}
```