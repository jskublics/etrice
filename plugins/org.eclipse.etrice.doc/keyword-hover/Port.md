A Port is an instance of a ProtocolClass and the interface for an ActorClass

```room
/** generated doc */
Port portName : ProtocolClass1
/** generated doc */
Port portName[4] : ProtocolClass1
/** generated doc */
Port conjugated portName : ProtocolClass1
```