A ProtocolClass defines messages and is the interface specification for a Port

```room
// prefixes: (eventdriven, datadriven, sync)
/** An eventdriven ProtocolClass by default */
ProtocolClass ProtocolName {
	usercode1 ''' '''
	usercode2 ''' '''
	usercode3 ''' '''
	incoming {
		Message messageName()
		Message messageName(int32)
	}
	outgoing {
		Message ...
	}
	regular PortClass {
		usercode1 ''' '''
		Attribute ...
		Operation ...
	}
	conjugated PortClass {
		...
	}
}

ProtocolClass ProtocolName extends ProtocolSuperClassName {
	// inherits all elements from super class
}
```