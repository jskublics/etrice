An actor is the basic structural building block for building systems with ROOM

```room
// prefixes: abstract and (eventdriven, datadriven, async, sync)
/** An eventdriven ActorClass by default */
ActorClass ActorName {
	Interface {
		Port port1 ...
		SPP spp1 ...
	}
	/** Documentation */
	Structure {
		usercode1 ''' '''
		usercode2 ''' '''
		usercode3 ''' '''
		external Port port1
		Port internalPort2 ...
		ActorRef ...
		Attribute ...
		
		ServiceImplementation of spp1
		Binding ...
		LayerConnection ...
	}
	/** Documentation */
	Behavior {
		ctor ''' /* constructor */ '''
		dtor ''' /* destructor */ '''
		Operation ...
		StateMachine ...
	}
}
```