A Binding connects two Ports with each other

```room
Binding port1 and actorRef.port2
Binding actorRef1.port1 and actorRef2.port2
```