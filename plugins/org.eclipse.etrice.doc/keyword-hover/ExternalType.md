An ExternalType is used to make an target language type accessible in ROOM

```room
ExternalType voidType -> "void" default "NULL"

/** Documentation */
ExternalType typeName -> "targetName" default "value"
```