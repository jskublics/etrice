# TestFeatures

| Features |     |
| -------- | --- |
| Contains: | [Feature1], [Feature2], [Feature3] |

## Feature1

lorem ipsum

| Features      |            |
| ------------- | ---------- |
| Is a:         | [Feature2] |
| Is of type:   | [Feature2] |
| Contains:     | [Feature2] |
| Uses:         | [Feature2] |
| Views:        | [Feature2] |
| Edits:        | [Feature2] |

| Feature Usage |               |
| ------------- | ------------- |
| Is used by:   | [Asdf]        |   <!-- error: unknown feature -->
| Qwer:         | [Feature3]    |   <!-- error: unrecognized relation type -->

## Feature2

lorem ipsum

| Feature Usage         |            |
| --------------------- | ---------- |
| Inheriting features:  | [Feature1] |
| Typecasts:            | [Feature1] |
| Is contained in:      | [Feature1] |
| Is used by:           | [Feature1] |
| Is shown by:          | [Feature1] |
| Is edited by:         | [Feature1] |

## Feature3

lorem ipsum

| Features  |            |
| --------- | ---------- |
| Views:    | [Feature2] |  <!-- error: missing opposite relation -->


[Feature1]: #feature1
[Feature2]: #feature2
[Feature3]: #feature3
[Asdf]: #asdf
