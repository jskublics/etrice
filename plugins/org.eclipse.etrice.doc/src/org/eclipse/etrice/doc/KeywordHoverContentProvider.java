/*******************************************************************************
 * Copyright (c) 2015 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.doc;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.etrice.core.common.ui.hover.IKeywordHoverContentProvider;
import org.eclipse.xtext.util.Files;

public class KeywordHoverContentProvider implements IKeywordHoverContentProvider {

	private static final String ROOM_LANGUAGE_REFERENCE = "docs/reference/room-language.md";
	private static final String KEYWORD_HOVER_FOLDER = "keyword-hover/";

	private final Map<String, String> keywordMdCache = new HashMap<String, String>();
	private String documentMdCache = null;

	@Override
	public String getMdContent(String name) {
		if (!ETriceHelp.DEV_MODE && keywordMdCache.containsKey(name)) {
			return keywordMdCache.get(name);
		}
		
		String result = null;
		
		// 1. try keyword hover help file
		try {
			URL fileURL = ETriceHelp.getDefault().getBundle().getEntry(KEYWORD_HOVER_FOLDER + name + ".md");
			if(fileURL != null) {
				result =  Files.readStreamIntoString(fileURL.openStream()).replace("\r\n", "\n");
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		// 2. try reference documentation
		if(result == null) {
			String documentation = getReferenceDocument();
			Pattern headingPattern = Pattern.compile("^ *(#+) +" + Pattern.quote(name) + "\\s+(.*)", 
					Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			Matcher startMatcher = headingPattern.matcher(documentation);
			if(startMatcher.find()) {
				int headingLevel = startMatcher.group(1).length();
				Pattern endPattern = Pattern.compile("(.*?)^ *#{1," + headingLevel + "}+\\s+", Pattern.MULTILINE | Pattern.DOTALL);
				String content = startMatcher.group(2).trim();
				Matcher endMatcher = endPattern.matcher(content);
				if(endMatcher.find()) {
					result = endMatcher.group(1).trim();
				} else {
					result = content;
				}
			}
		}

		keywordMdCache.put(name, result);
		return result;
	}
	
	private String getReferenceDocument() {
		if(documentMdCache == null) {
			try {
				URL fileURL = ETriceHelp.getDefault().getBundle().getEntry(ROOM_LANGUAGE_REFERENCE);
				documentMdCache = Files.readStreamIntoString(fileURL.openStream()).replace("\r\n", "\n");
			} catch(IOException e) {
				e.printStackTrace();
				documentMdCache = "";
			}
		}
		return documentMdCache;
	}
}
