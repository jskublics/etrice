Prism.languages.etconfig = {
	'comment': {
		pattern: /(?:\/\*[\s\S]*?\*\/)|(?:\/\/.*)/,
		greedy: true
	},
	'string': {
		pattern: /"(?:\\(?:\r\n|[\s\S])|[^"\\\r\n])*"/,
		greedy: true
	},
	'punctuation': /[{}/,:]/,
    'operator': /=/,
	'number': /\b\d+\b/,
	'keyword': /\b(?:ActorClassConfig|ActorInstanceConfig|Attr|ConfigModel|InterfaceItem|Port|ProtocolClassConfig|SubSystemConfig|attribute|conjugate|conjugated|dynamic|configuration|file|path|from|import|mandatory|max|min|model|optional|polling|interval|read|regular|user|constructor|user|import|write)\b/
};
