Enumerations
============

Another top level type that eTrice introduces is the `Enumeration`. Enumerations are well known from many programming languages. They basically are a lists of literals, each of which is a pair consisting of a name and an integer constant.

The assignment of integer constants is optional in eTrice. If no value is specified then the value is that of the predecessor plus one or 0 if there is no predecessor.

The default type of the enumeration is an `int` and depends on the target platform. But it is also possible to explicitly associate a `PrimitiveType` (of integer type of course) with the enumeration.

In the following listing we show a couple of examples for enumerations.
