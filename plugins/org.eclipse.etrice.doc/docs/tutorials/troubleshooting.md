Troubleshooting
===============

If the application fails to start, look for problems in the code generation and in the build process. Check the respective console for potential errors:

## Generation error

The *eTrice Generator Console* outputs any errors, that were detected during generation, like model validation errors or missing imports/references.

![image](../images/020-gen-console.png)

## Build error

The CDT Build Console outputs errors that occurred during the build process.

Common issues:

-   *multiple main functions*: More than one executable application was built within a single project. Try a complete clean before rebuild of the project.

-   compile error in generated user code: Check if the user code, that was generated out of the model causes compiler errors (e.g. state/transition action code or operation detail code).
The default location for the generated code is the folder *src-gen*.

![image](../images/020-build-console.png)

## Missing MSC

The MSC is created when the application has been shutdown in proper form, thus has been terminated by typing *quit* in the Console of the application.
Depending on the Eclipse workspace settings, it might be necessary to refresh (F5) the *log* folder manually.
