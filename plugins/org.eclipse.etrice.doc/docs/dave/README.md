DAVE-eTrice Toolchain Tutorial
==============================

Introduction
------------

As you already know, eTrice is a ROOM-Tool that provides a high level modeling language for embedded systems. It is perfectly suited for event driven, real time systems. However, each embedded SW relies on an underlying HW, with components like digital I/Os, Sensors, ADCs, DACs, PWMs and so on to connect the real world. Therefore some driver SW is required to control all this HW components and to provide easy access for the higher level SW. To develop the HW-drivers as well as your application logic without changing the development environment, you need a tool chain that provides both, driver development and high level application development. For Infineon's XMC&trade; ARM Cortex M0/M4 devices special support is provided to combine the DAVE&trade; tool for driver development with eTrice for application development. This tutorials will guide you through the first steps.

The tutorials relies on the XMC4700 Relax Lite Kit. As a precondition you should be familiar with DAVE&trade;. It is very easy to adapt the tutorials to any other development board or to your own HW.           
