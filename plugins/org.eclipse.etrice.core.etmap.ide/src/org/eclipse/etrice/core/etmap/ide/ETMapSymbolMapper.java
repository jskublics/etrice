package org.eclipse.etrice.core.etmap.ide;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.xtext.ide.server.symbol.DocumentSymbolMapper;

/** See RoomSymbolMapper */
public class ETMapSymbolMapper {
	public static class ETMapSymbolKindProvider extends DocumentSymbolMapper.DocumentSymbolKindProvider {
		@Override
		protected SymbolKind getSymbolKind(EClass clazz) {
			// show no symbols for etmap files
			return null;
		}
	}
}
