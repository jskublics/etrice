/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

import {SNodeImpl, SPortImpl} from "sprotty";

/** 
 * Sprotty representation of an interface eTrice Port
 */
export class SInterfacePort extends SPortImpl {
    isConjugated: boolean = false;
    isRelayPort: boolean = false;
    multiplicity: number = 0;
}

/**
 * Sprotty representation of an internal eTrice Port
 */
export class SInternalPort extends SNodeImpl {
    isConjugated: boolean = false;
    isRelayPort: boolean = false;
    multiplicity: number = 0;
}

/**
 * Sprotty representation of an eTrice ActorReference
 */
export class SActorRef extends SNodeImpl {
    fileURI : string = "";
    actorName : string = "";
}

/**
 * Sprotty representation of an eTrice Actor
 */
export class SActorClass extends SNodeImpl {
    
}