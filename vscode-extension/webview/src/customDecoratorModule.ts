/*******************************************************************************
 * Copyright (c) 2025 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

import { ContainerModule } from "inversify";
import { configureModelElement, DecorationPlacer, SIssueMarkerImpl, TYPES } from "sprotty";
import { CustomIssueMarkerView } from "./views";

const customDecorationModule = new ContainerModule((bind, _unbind, isBound)  => {
    configureModelElement({ bind, isBound }, 'marker', SIssueMarkerImpl, CustomIssueMarkerView);
    bind(DecorationPlacer).toSelf().inSingletonScope();
    bind(TYPES.IVNodePostprocessor).toService(DecorationPlacer);
});

export default customDecorationModule;