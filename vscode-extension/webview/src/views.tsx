/*******************************************************************************
 * Copyright (c) 2025 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

/** @jsx svg */

import { injectable } from 'inversify';
import { VNode } from 'snabbdom';
import { ShapeView, svg, RenderingContext, SIssueMarkerImpl, setClass } from 'sprotty';
import { SIssueSeverity } from 'sprotty-protocol'
import { SErrorMessage } from './model';

@injectable()
export class ErrorMessageView extends ShapeView {
    render(error: Readonly<SErrorMessage>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(error,context)) {
            return undefined;
        }
        return <g>
            <rect class-sprotty-diagram-error={true}
                  x="0" y="0" width={Math.max(7 * error.errorMessage.length, 0)} height={Math.max(error.size.height, 0)}>
            </rect>
            <text class-sprotty-diagram-error-message={true} x="10" y={error.size.height / 2}>
                {error.errorMessage}
            </text>
                  {context.renderChildren(error)}
        </g>;
    }
}

/*
    Class copied from sprotty\src\features\decoration\views.tsx
    Modified Line:
        <circle class-sprotty-issue-background={true} cx="768" cy="896" r="700"/>
    Issue:
        requestPopupModel Action was not triggered when the cursor was hovering directly over the exclamation point of an Issue Marker
    Solution:
        Add a black circle as a backgorund behind the Issue Marker to fill the hoverable area
*/
@injectable()
export class CustomIssueMarkerView extends ShapeView {
    render(marker: SIssueMarkerImpl, context: RenderingContext): VNode {
        const scale = 16 / 1792;
        const trafo = `scale(${scale}, ${scale})`;
        const maxSeverity = this.getMaxSeverity(marker);
        const group = <g class-sprotty-issue={true}>
            <g transform={trafo}>
                <circle class-sprotty-issue-background={true} cx="768" cy="896" r="700"/>
                <path d={this.getPath(maxSeverity)} />
            </g>
        </g>;
        setClass(group, 'sprotty-' + maxSeverity, true);
        return group;
    }

    protected getMaxSeverity(marker: SIssueMarkerImpl): SIssueSeverity {
        let currentSeverity: SIssueSeverity = 'info';
        for (const severity of marker.issues.map(s => s.severity)) {
            if (severity === 'error' || (severity === 'warning' && currentSeverity === 'info'))
                currentSeverity = severity;
        }
        return currentSeverity;
    }

    protected getPath(severity: SIssueSeverity) {
        switch (severity) {
            case 'error':
            case 'warning':
                // eslint-disable-next-line max-len
                return "M768 128q209 0 385.5 103t279.5 279.5 103 385.5-103 385.5-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103zm128 1247v-190q0-14-9-23.5t-22-9.5h-192q-13 0-23 10t-10 23v190q0 13 10 23t23 10h192q13 0 22-9.5t9-23.5zm-2-344l18-621q0-12-10-18-10-8-24-8h-220q-14 0-24 8-10 6-10 18l17 621q0 10 10 17.5t24 7.5h185q14 0 23.5-7.5t10.5-17.5z";
            case 'info':
                // eslint-disable-next-line max-len
                return "M1024 1376v-160q0-14-9-23t-23-9h-96v-512q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v160q0 14 9 23t23 9h96v320h-96q-14 0-23 9t-9 23v160q0 14 9 23t23 9h448q14 0 23-9t9-23zm-128-896v-160q0-14-9-23t-23-9h-192q-14 0-23 9t-9 23v160q0 14 9 23t23 9h192q14 0 23-9t9-23zm640 416q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z";
        }
    }
    
}