/*******************************************************************************
 * Copyright (c) 2025 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

/** @jsx svg */

import { injectable } from 'inversify';
import { VNode } from 'snabbdom';
import { BezierCurveEdgeView, IViewArgs, RenderingContext, SEdgeImpl, SGraphView, ShapeView, SLabelImpl, SNodeImpl, SPortImpl, svg } from "sprotty";
import { Point, toDegrees } from 'sprotty-protocol';
import { SState, STransitionPoint } from './model';


@injectable()
export class BehaviorDiagramView extends SGraphView {
    
}

@injectable()
export class StateMachineView extends ShapeView {

    render(stateMachine: Readonly<SNodeImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(stateMachine, context)) {
            return undefined;
        }
        return <g>
            <rect x="0" y="0" width={Math.max(stateMachine.size.width, 0)} height={Math.max(stateMachine.size.height, 0)}
                  class-sprotty-state-machine={true}>
            </rect>
            {context.renderChildren(stateMachine)}
        </g>

    }
}

@injectable()
export class StateView extends ShapeView {
    
    render(state: Readonly<SState>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(state,context)) {
            return undefined;
        }
        
        return <g>
            <rect class-sprotty-state={true}
                  x="0" y="0" width={Math.max(state.size.width, 0)} height={Math.max(state.size.height, 0)} rx="10">
            </rect>
            {context.renderChildren(state)}
        </g>
    }
}

@injectable()
export class SubGraphIndicatorView extends ShapeView {

    render(label: Readonly<SLabelImpl>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(label,context)) {
            return undefined;
        }

        return <g>
            <rect class-sprotty-subGraph-indicator={true}
                  x="0" y="0"
                  width="15" height="10" rx="4">
            </rect>
            {context.renderChildren(label)}
        </g>
    }
}

@injectable()
export class InitialPointView extends ShapeView {

    render(state: Readonly<SNodeImpl>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(state,context)) {
            return undefined;
        }
        return <g>
            <circle cx="50" cy="50" r="50" fill="white" stroke="black" stroke-width="8px" transform={"scale(" + state.size.width / 100 + ")"}/>
            <path d= "M 50 25 l 0 50 Z" stroke="black" stroke-width="2px" transform={"scale(" + state.size.width / 100 + ")"}/>
        </g>
    }
}

@injectable()
export class ChoicePointView extends ShapeView {
    render(choicePoint: Readonly<SNodeImpl>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(choicePoint,context)) {
            return undefined;
        }
        return <g>
            <circle cx="50" cy="50" r="50" fill="white" stroke="black" stroke-width="8px" transform={"scale(" + choicePoint.size.width / 100 + ")"}/>
            <path d= "M 60 25 a 25 27 0 1 0 1 50" stroke="black" fill="none" stroke-width="2px" transform={"scale(" + choicePoint.size.width / 100 + ")"}/>
        </g>
    }
}

@injectable()
export class TransitionPointView extends ShapeView {
    render(transitionPoint: Readonly<STransitionPoint>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(transitionPoint, context)) {
            return undefined;
        }
        return <g>
            <circle class-sprotty-transitionPoint={!transitionPoint.isHandler}
                    class-sprotty-transitionPoint-handler={transitionPoint.isHandler}
                    cx="50" cy="50" r="50" stroke="black" stroke-width="5px" transform={"scale(" + transitionPoint.size.width / 100 + ")"}/>
        </g>
    }
}

@injectable()
export class EntryPointView extends ShapeView {
    render(entryPoint: Readonly<SPortImpl>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(entryPoint, context)) {
            return undefined;
        }
        return <g>
            <circle cx="50" cy="50" r="50" fill="white" stroke="black" stroke-width="4px" transform={"scale(" + entryPoint.size.width / 100 + ")"}/>
            <path d="M 15 15 L 85 85 M 85 15 L 15 85 Z" stroke="black" fill="none" stroke-width="4px" transform={"scale(" + entryPoint.size.width / 100 + ")"}/>
        </g>
    }
}

@injectable()
export class ExitPointView extends ShapeView {
    render(exitPoint: Readonly<SPortImpl>, context: RenderingContext, args?: IViewArgs): VNode | undefined {
        if (!this.isVisible(exitPoint, context)) {
            return undefined;
        }
        return <g>
            <circle cx="50" cy="50" r="50" fill="black" stroke="black" stroke-width="4px" transform={"scale(" + exitPoint.size.width / 100 + ")"}/>
            <path d="M 50 5 L 5 50 L 50 95 L 95 50 Z" stroke="none" fill="white" transform={"scale(" + exitPoint.size.width / 100 + ")"}/>
        </g>
    }
}

@injectable()
export class PolylineArrowEdgeView extends BezierCurveEdgeView {

    protected override renderAdditionals(edge: SEdgeImpl, segments: Point[], context: RenderingContext): VNode[] {
        const p1 = segments[segments.length - 2]!
        const p2 = segments[segments.length - 1]!
        return [
            <path class-sprotty-edge-arrow={true} d="M 10,-4 L 0,0 L 10,4 Z"
                  transform={`rotate(${this.angle(p2, p1)} ${p2.x} ${p2.y}) translate(${p2.x} ${p2.y})`}/>
        ]
    }

    angle(x0: Point, x1: Point): number {
        return toDegrees(Math.atan2(x1.y - x0.y, x1.x - x0.x))
    }
}

@injectable()
export class EdgeLabelView extends ShapeView {
    override render(label: Readonly<SLabelImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(label, context)) {
            return undefined;
        }
        return <g>
            <text class-sprotty-edgeLabel={true} x={- label.size.width / 2}>{label.text}</text>
        </g>
    }
}