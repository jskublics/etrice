/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 
 *******************************************************************************/

import 'reflect-metadata';
import 'sprotty-vscode-webview/css/sprotty-vscode.css';

import { SprottyDiagramIdentifier } from 'sprotty-vscode-webview';
import { SprottyLspEditStarter } from 'sprotty-vscode-webview/lib/lsp/editing';
import { createStructureDiagramContainer } from './structure/di.config';
import { createBehaviorDiagramContainer } from './behavior/di.config';


export class StructureSprottyStarter extends SprottyLspEditStarter {

    createContainer(diagramIdentifier: SprottyDiagramIdentifier) {
        if (diagramIdentifier.diagramType === 'behavior-diagram') {
            return createBehaviorDiagramContainer(diagramIdentifier.clientId);
        } else if(diagramIdentifier.diagramType == 'structure-diagram') {
            return createStructureDiagramContainer(diagramIdentifier.clientId);
        }
        throw new Error("unknown diagram type: " + diagramIdentifier.diagramType);
    }
}

new StructureSprottyStarter().start();
