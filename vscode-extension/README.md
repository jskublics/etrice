# eTrice VSCode Extension

🚧 This extension is a preview.

## Installation
 * Build the extension packaged as vsix file with `../gradlew vscePackage`.
 * Or download the vsix file from: `https://download.eclipse.org/etrice/nightly/archives/`
 * Use the command `Install from VSIX` in Visual Studio Code to install the extension.

## Developer Setup
 * run `gradlew installDependencies` in the etrice repository to download nesseccary Sprotty Modules
 * run `gradlew runLanguageServer` in the etrice repository to start the Language Server
 * run `npm install` in the vscode-extension folder
 * To use the locally running etrice language server select `socketServerOptions` in `createLanguageClient` function in `src/extension.ts` by replacing `const serverOptions: ServerOptions = localServerOptions(context);` with `const serverOptions: ServerOptions = socketServerOptions();`.
 * run `npm run build` in the vscode-extension folder and in the vscode-extension/webview folder to build the vscode extension
 * start the vscode extension through the vscode debug menu while the language server is running

## Usage
Make sure that there is a modelpath description file in every eTrice project directory.

## Features
 * Basic syntax highlighting
 * eTrice language server
 * Structure Diagrams
    * Open Actor in Diagram:     Press 'strg + shift + p' to access the vscode command palett and search for 'Open Structure Diagram' to open a single Actor based on Cursor Position
    * Movement:                  Hold 'middle or right mouse button' and drag to move the diagram
    * Highlight in Diagram:      'Left click' an eTrice Element in the text editor to highlight it in the diagram - LayerConnections are currently not supported
    * Highlight in Text Editor:  'Left click' an eTrice Element in the diagram to highlight it in the text editor
    * Open in Text Editor:       'Double left click' on an eTrice Element to open the corresponding Text Document
    * Open ActorRef in Diagram:  'shift + Double left click' on an ActorRef to open the structure diagram of the Actor
    * Undo open ActorRef:        'shift + Double left click' on an Actor to undo the last open ActorRef action
 * Behavior Diagrams
    * Open Behavior Diagram:     Press 'strg + shift + p' to access the vscode command palett and search for 'Open Behavior Diagram' to open the topmost StateMachine of an Actor
    * Hierarchical state machines and inheritance are not yet supported!
