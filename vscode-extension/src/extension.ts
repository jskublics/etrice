/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Julian Skublics
 * 
 *******************************************************************************/

'use strict';

import * as path from 'path';
import * as os from 'os';
import * as net from 'net';
import * as vscode from 'vscode';
import * as fs from 'fs'
import * as unzip from 'extract-zip'
import { Downloader } from 'nodejs-file-downloader'

import { LanguageClient, LanguageClientOptions, ServerOptions, StreamInfo } from 'vscode-languageclient/node';
import { SprottyDiagramIdentifier, createFileUri, createWebviewPanel, registerDefaultCommands } from 'sprotty-vscode';
import { LspWebviewPanelManager, LspWebviewPanelManagerOptions, OpenInTextEditorMessage, convertPosition, openInTextEditorMessageType } from 'sprotty-vscode/lib/lsp';

let languageClient: LanguageClient;

export async function activate(context: vscode.ExtensionContext) {
    if (os.platform() !== 'win32') {
        vscode.window.showErrorMessage("At the moment the eTrice vscode-extension only supports Windows!");
        return;
    }
    let javaHome : string | undefined;

    javaHome = vscode.workspace.getConfiguration('etrice').get('javaPath');

    if (!javaHome || !fs.existsSync(javaHome)) {
        if (!fs.existsSync(context.asAbsolutePath(path.join('etrice','jre17')))) {
            if (await showJavaPathNotification(context) === 'setJavaPath') {
                javaHome = vscode.workspace.getConfiguration('etrice').get('javaPath');
            } else {
                javaHome = context.asAbsolutePath(path.join('etrice', 'jre17'));
            }
        } else {
            javaHome = context.asAbsolutePath(path.join('etrice', 'jre17'));
        }
    }

    languageClient = createLanguageClient(context, javaHome!);
    const webviewPanelManager = new DiagramWebviewPanelManager({
        extensionUri: context.extensionUri,
        languageClient,
        supportedFileExtensions: ['.room']
        
    });
    registerDefaultCommands(webviewPanelManager, context, { extensionPrefix: 'structure' });
    registerDefaultCommands(webviewPanelManager, context, { extensionPrefix: 'behavior' });

    const openBehaviorCommand = () => {
        const editor = vscode.window.activeTextEditor;
        if (editor == null) {
            return
        }
        const offset = editor.document.offsetAt(editor.selection.active);
        webviewPanelManager.openDiagram(editor.document.uri.with({query: 'cursorOffset=' + offset.toString()}), {reveal: true, diagramType: 'behavior-diagram'});
    }

    const openActorCommand = () => {
        const editor = vscode.window.activeTextEditor;
        if (editor == null) {
            return;
        }

        /* 
         * The Information wich Actor should be opened is determined by the Position of the Cursor in the TextDocument
         * Because of the internal implementation of Sprotty the RequestOptions of the first Request can not be edited directly
         * To circumvent that the cursorOffset is added as a URI query String
        */
        var offset = editor.document.offsetAt(editor.selection.active);
        webviewPanelManager.openDiagram(editor.document.uri.with({query: 'cursorOffset=' + offset.toString()}), { reveal: true, diagramType: 'structure-diagram' });
    }
    context.subscriptions.push(vscode.commands.registerCommand('structure.diagram.openActor', openActorCommand));
    context.subscriptions.push(vscode.commands.registerCommand('behavior.diagram.openBehavior', openBehaviorCommand));
}

async function showJavaPathNotification(context: vscode.ExtensionContext) : Promise<String> {
    const javaPathNotification = await vscode.window.showInformationMessage("Set the Java Path setting or simply download a jre", "Download jre", "Set Java Path");
    if (javaPathNotification === "Set Java Path") {
        const specifiedPath = await vscode.window.showOpenDialog({
            canSelectMany: false,
            canSelectFolders: true
        });
        if (!specifiedPath) {
            return "error";
        }
        const javaHome = specifiedPath.at(0);
        vscode.workspace.getConfiguration('etrice').update('javaPath', javaHome?.fsPath, true);
        return 'setJavaPath';
    } else {
        await vscode.window.withProgress({
            location: vscode.ProgressLocation.Notification,
            title: "Installing Dependencies"
        }, (progress, token) => {
            return installJava(progress, context);
        })
        return "downloadJre"
    }
}


async function installJava(progress: vscode.Progress<{ message?: string; increment?: number }>, context: vscode.ExtensionContext): Promise<void> {
    fs.rmSync(context.asAbsolutePath(path.join('etrice','jre')), {recursive: true, force: true});
    progress.report({message: "Installing jre17. Please wait"});
    const url = os.platform() === 'win32' ?
            "https://api.adoptium.net/v3/binary/latest/17/ga/windows/x64/jre/hotspot/normal/eclipse" : 
            "https://api.adoptium.net/v3/binary/latest/17/ga/linux/x64/jre/hotspot/normal/eclipse";
    const filename = "jdk.zip"
    const downloader = new Downloader({
        url: url,
        fileName: filename,
        directory: context.asAbsolutePath(path.join('etrice'))
    });
    try {
        const {filePath,downloadStatus} = await downloader.download();
        try {
            progress.report({message: "Unpacking zip. Please wait"});
            await unzip(filePath!, {dir: context.asAbsolutePath(path.join('etrice', 'jre'))})
            fs.unlinkSync(filePath!);
            progress.report({message: "Installing jre17: Done"});
            
            const javaFolder = fs.readdirSync(context.asAbsolutePath(path.join('etrice','jre'))).at(0);
            fs.renameSync(context.asAbsolutePath(path.join('etrice','jre',javaFolder!)), context.asAbsolutePath(path.join('etrice','jre17')));
            fs.rmdirSync(context.asAbsolutePath(path.join('etrice','jre')));
        } catch (err) {
            progress.report({message: "Unpacking zip failed"})
            console.log("Unpacking zip failed", err)
        }
    } catch (error) {
        progress.report({message: "Error while downloading Java"})
        console.log("Download failed", error);
    }
}

export async function deactivate(): Promise<void> {
    if (languageClient) {
        await languageClient.stop();
    }
}

function createLanguageClient(context: vscode.ExtensionContext, javaHome: string): LanguageClient {
    const serverOptions: ServerOptions = localServerOptions(context, javaHome);
    const clientOptions: LanguageClientOptions = {
        documentSelector: ['room', 'etphys', 'etmap', 'cage'],
        synchronize: {
            fileEvents: vscode.workspace.createFileSystemWatcher('**/*')
        }
    };
    const languageClient = new LanguageClient('etrice', 'eTrice Language Server', serverOptions, clientOptions);
    languageClient.start();
    return languageClient;
}

class DiagramWebviewPanelManager extends LspWebviewPanelManager {
    constructor(options: LspWebviewPanelManagerOptions) {
        super(options);
        options.languageClient.onNotification(openInTextEditorMessageType, message => this.openInTextEditorWorkaround(message));
    }

    /*
        Mehtod copied from lsp-utils.ts
        Modified Line:
            const editor = vscode.window.visibleTextEditors.find(ed => ed.document.uri.toString() === message.location.uri);
        Issue:
            Parts of message.location.uri contain encoded characters ('%3A' instead of ':'). Because of that the comparison between the Strings always evaluates to 'false'
        Solution:
            Parse message.location.uri to decode any encoded characters and compare the String representation of the uri afterwards
    */
    private openInTextEditorWorkaround(message: OpenInTextEditorMessage): void {

        const editor = vscode.window.visibleTextEditors.find(ed => ed.document.uri.toString() === vscode.Uri.parse(message.location.uri).toString());

        if (editor) {
            const start = convertPosition(message.location.range.start);
            const end = convertPosition(message.location.range.end);
            editor.selection = new vscode.Selection(start, end);
            editor.revealRange(editor.selection, vscode.TextEditorRevealType.InCenter);
        } else if (message.forceOpen) {
            vscode.window.showTextDocument(vscode.Uri.parse(message.location.uri), {
                selection: new vscode.Range(convertPosition(message.location.range.start),
                                            convertPosition(message.location.range.end)),
                viewColumn: vscode.ViewColumn.One
            });
        }
    }

    protected override createWebview(identifier: SprottyDiagramIdentifier): vscode.WebviewPanel {
        const extensionPath = this.options.extensionUri.fsPath;
        const panel = createWebviewPanel(identifier, {
            localResourceRoots: [ createFileUri(extensionPath, 'webview', 'pack') ],
            scriptUri: createFileUri(extensionPath, 'webview', 'pack', 'webview.js')
        });
        return panel;
    }


}

// The server is a locally installed in the directory 'etrice'
function localServerOptions(context: vscode.ExtensionContext, javaHome: string): ServerOptions {
    let launcher = os.platform() === 'win32' ? 'etrice-ls.bat' : 'etrice-ls';
    let script = context.asAbsolutePath(path.join('etrice', 'bin', launcher));
    let env = Object.assign({}, process.env);
    env['JAVA_HOME'] = javaHome;
    return {
        run: { command: script, options: {env: env, shell: true}},
        debug: { command: script, args: [], options: { env: env, shell: true } }
    };
}

function createDebugEnv() {
    return Object.assign({
        JAVA_OPTS: "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,quiet=y,address=5005"
    }, process.env);
}

// The server is a started as a separate app and listens on port 5008
function socketServerOptions(): ServerOptions {
    let connectionInfo = {
        port: 5008
    };
    return () => {
        // Connect to language server via socket
        let socket = net.connect(connectionInfo);
        let result: StreamInfo = {
            writer: socket,
            reader: socket
        };
        return Promise.resolve(result);
    };
}