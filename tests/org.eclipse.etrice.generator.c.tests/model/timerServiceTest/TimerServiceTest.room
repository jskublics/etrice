/**
 * Functional tests for etrice.api.timer.ATimerService and related etrice model API.
 * 
 * NOTE: These tests extends the ATimerService class to allow setting simulated time for testing.
 */
RoomModel TimerServiceTest {

	import etrice.api.types.*
	import etrice.api.timer.ATimerService
	import etrice.api.timer.PTimer
	import etrice.api.timer.targetTime
	import etrice.api.testcontrol.PTestControl
	import etrice.api.testcontrol.ControllableSequentialTestExecutor

	/**
	 * Extended ATimerService used by the test cases. Overrides getTime Operation so that simulated
	 * target time can be injected into the ATimerService. Time can be set with the added
	 * PTimeTestCtl SPP. The reason why this is not just done in the actor StateMachine is to avoid
	 * overriding internal StateMachine behavior of the ATimerService under test.
	 * 
	 * NOTE: Only one PTimeTestCtl SAP should be active at any given time. The getTime operation
	 * will cycle through all SAPs and select the time from the first enabled one.
	 * 
	 */
	async ActorClass ATimerServiceOverrideGetTime extends ATimerService {
		Interface {
			SPP testTimeCtl: PTestTimeControl
		}
		Structure {
			ServiceImplementation of testTimeCtl
		}
		Behavior {
			override Operation getTime(t: targetTime ref) '''
				t->sec = 0;
				t->nSec = 0;
				PTestTimeControlConjReplPort* replPort = (PTestTimeControlConjReplPort*)&self->constData->testTimeCtl;
				etInt32 numPorts = PTestTimeControlConjReplPort_getReplication(replPort);
				for (etInt32 i = 0; i < numPorts; ++i) {
					PTestTimeControlConjPort* port = (PTestTimeControlConjPort*)&replPort->ports[i].port;
					if (PTestTimeControlPort_isTestControlEnabled(port)) {
						PTestTimeControlPort_getTestControlTime(port, t);
						break;
					}
				}
			'''
		}
	}

	/**
	 * ATimerService should send a timeout once per the expected number of polling intervals,
	 * given the timer interval specified with setTimer.
	 */
	async ActorClass APolledPeriodicTimeoutTest extends APolledTimerServiceTestBase {
		Behavior {
			override Operation getTestName(): string '''
				return "ShouldSendPeriodicTimeoutsWithinExpectedPollingIntervals";
			'''
			override Operation getExpectedEvents(): DTestEvents '''
				static int16 expectedTestEvents[] = {
					ETestEvent.POLLING_CYCLE,	// given polling interval 300ms, startTimer 500ms at >0ms
					ETestEvent.POLLING_CYCLE,
					ETestEvent.POLLING_CYCLE,
					ETestEvent.TIMEOUT,			// timeout at 500ms -> queued during polling cycle 3
					ETestEvent.POLLING_CYCLE,
					ETestEvent.POLLING_CYCLE,
					ETestEvent.TIMEOUT,			// timeout at 1000ms -> queued during polling cycle 5
					ETestEvent.POLLING_CYCLE,
					ETestEvent.END,
				};
				DTestEvents result = {
					.elements = expectedTestEvents,
					.size = sizeof(expectedTestEvents)/sizeof(expectedTestEvents[0]),
				};
				return result;
			'''
			override Operation doTestSequenceStep(cycle: int32) '''
				if (cycle == 0) {
					// Initiate test sequence, synchronized to polling interval.
					etLogger_logInfo("sending startTimer request");
					timer.startTimer(500);
				}
			'''
			override Operation getSimTimeIncrement(cycle: int32): targetTime '''
				etTime increment = {0, 300 * 1000000}; // 300ms
				return increment;
			'''
		}
	}

	/**
	 * ATimerService should drop any periodic timeouts that are scheduled before the current time.
	 */
	async ActorClass ADroppedPeriodicTimeoutTest extends  APolledTimerServiceTestBase {
		Behavior {
			override Operation getTestName(): string '''
				return "ShouldDropPeriodicTimeoutsScheduledBeforeCurrentTargetTime";
			'''
			override Operation getExpectedEvents(): DTestEvents '''
				static int16 expectedTestEvents[] = {
					ETestEvent.POLLING_CYCLE,
					ETestEvent.POLLING_CYCLE,
					// timeout at 100ms -> scheduled at polling cycle 300ms, next timeout at 
					// 200ms is in the past, so should be dropped and rescheduled for 400ms
					ETestEvent.TIMEOUT,
					ETestEvent.POLLING_CYCLE,
					// timeout at 400ms -> scheduled at polling cycle 600ms, next timeout at 
					// 500ms is in the past, so should be dropped and rescheduled for 700ms
					ETestEvent.TIMEOUT,
					ETestEvent.POLLING_CYCLE,
					ETestEvent.END,
				};
				DTestEvents result = {
					.elements = expectedTestEvents,
					.size = sizeof(expectedTestEvents)/sizeof(expectedTestEvents[0]),
				};
				return result;
			'''
			override Operation doTestSequenceStep(cycle: int32) '''
				if (cycle == 0) {
					// Initiate test sequence, synchronized to polling interval.
					etLogger_logInfo("sending startTimer request");
					timer.startTimer(100);
				}
			'''
			override Operation getSimTimeIncrement(cycle: int32): targetTime '''
				etTime increment = {0, 300 * 1000000}; // 300ms
				return increment;
			'''
		}
	}

	ActorClass ATestHarness {
		Interface {
		}
		Structure {
			usercode2 '''
				#include <etUnit/etUnit.h>
				#include <osal/etSema.h>
				#include <runtime/etRuntime.h>
			'''
			conjugated Port runnerCtl: PTestControl
			ActorRef runner: ControllableSequentialTestExecutor
			ActorRef t1: APolledPeriodicTimeoutTest
			ActorRef t2: ADroppedPeriodicTimeoutTest
			Binding runner.control and t1.ctl
			Binding runner.control and t2.ctl
			Binding runner.exeControl and runnerCtl
		}
		Behavior {
			StateMachine {
				Transition init: initial -> Running {
					action '''
						etUnit_open("log", "TimerServiceTest");
						etUnit_openTestSuite("org.eclipse.etrice.generator.c.tests.TimerServiceTest");
						runnerCtl.start();
					'''
				}
				State Running
				Transition done: Running -> Finished {
					triggers {
						<done: runnerCtl>
					}
					action '''
						etUnit_closeTestSuite();
						etUnit_close();
						etSema_wakeup(etRuntime_getTerminateSemaphore());
					'''
				}
				State Finished
			}
		}
	}

	SubSystemClass TimerServiceTestSubSys {
		LogicalThread defaultThread

		ActorRef actorUnderTest: ATimerServiceOverrideGetTime
		ActorRef tests: ATestHarness
		LayerConnection ref tests satisfied_by actorUnderTest.timer
		LayerConnection ref tests satisfied_by actorUnderTest.supervisionControl
		LayerConnection ref tests satisfied_by actorUnderTest.testTimeCtl
	}
	
	LogicalSystem TimerServiceTestLogSys {
		SubSystemRef main: TimerServiceTestSubSys
	}

	/**
	 * The polling based ATimerService test cases all have a similar structure. The expectations are
	 * defined as a sequence of events. Each polling cycle, the test sequence will be advanced and
	 * a message will be sent to update simulated time injected into the ATimerService for the next
	 * cycle.
	 * 
	 * <p>Concrete test cases shall override the four Operations in the base actor behavior.</p> 
	 */
	abstract async ActorClass APolledTimerServiceTestBase {
		Interface {
			Port ctl: PTestControl
		}
		Structure {
			usercode2 '''
				#include <debugging/etMSCLogger.h>
				#include <etUnit/etUnit.h>
				#include <helpers/etTimeHelpers.h>
				#include "TimerServiceTest/ETestEvent.h"
			'''
			external Port ctl
			SAP testTimeCtl: PTestTimeControl
			SAP timer: PTimer
			Attribute testId: int16
			Attribute pollingCycles: int32
			Attribute timeouts: int32
			Attribute simTimeCurrent: targetTime // The sim time that the unit under test currently sees
			Attribute simTimeNew: targetTime // The new requested sim time
			Attribute dummyEvent: ETestEvent // included to allow C translation of . operation on ETestEvent type
		}
		Behavior {
			Operation getTestName(): string '''
				return "";
			'''

			// Define event expectations.
			// NOTE: the timeout messages are dispatched after the polling handlers
			// have been executed, which is why:
			// - the timeout events always follow after the polling cycle in which they were queued.
			// - the ATimerService only reflects new setTime changes in the *next* polling cycle.
			Operation getExpectedEvents(): DTestEvents '''
				DTestEvents e = {0};
				return e;
			'''

			Operation doTestSequenceStep(cycle: int32) '''
			'''

			Operation getSimTimeIncrement(cycle: int32): targetTime '''
				etTime t = {0};
				return t;
			'''

			StateMachine {
				Transition init: initial -> Pre
				State Pre
				Transition start: Pre -> Running {
					triggers {
						<start: ctl>
					}
					action '''
						testId = etUnit_openTestCase(getTestName());
						DTestEvents expectedEvents = getExpectedEvents();
						EXPECT_ORDER_START(testId, expectedEvents.elements, expectedEvents.size);
						testTimeCtl.enable();
						testTimeCtl.setTime(&simTimeNew);
					'''
				}
				State Running {
					do '''
						doTestSequenceStep(pollingCycles);

						// Increment simulated time
						simTimeCurrent = simTimeNew;
						etTime interval = getSimTimeIncrement(pollingCycles);
						etTimeHelpers_add(&simTimeNew, &interval);
						testTimeCtl.setTime(&simTimeNew);

						etLogger_logInfoF("polling cycle %d (timer service sees %d)", pollingCycles, etTimeHelpers_convertToMSec(&simTimeCurrent));
						EXPECT_ORDER(testId, "pollingCycle", ETestEvent.POLLING_CYCLE);
						++pollingCycles;
					'''
				}
				Transition recvTimeout: Running -> Running {
					triggers {
						<timeout: timer>
					}
					action '''
						++timeouts;
						etLogger_logInfoF("timeout %d (>%d)", timeouts, etTimeHelpers_convertToMSec(&simTimeCurrent));
						EXPECT_ORDER(testId, "timeout", ETestEvent.TIMEOUT);
					'''
				}
				Transition done: Running -> Post {
					guard '''timeouts == 2'''
					action '''
						// End test sequence
						etLogger_logInfo("testcase done");
						EXPECT_ORDER_END(testId, "done", ETestEvent.END);
						ctl.done(true);
					'''
				}
				Transition tooManyCycles: Running -> Post {
					guard '''pollingCycles > 30'''
					action '''
						EXPECT_TRUE(testId, "Too many polling cycles", ET_FALSE);
						ctl.done(false);
					'''
				}
				Transition abort: Running -> Post {
					triggers { <abort: ctl> }
					action '''
						EXPECT_TRUE(testId, "Aborted test", ET_FALSE);
						ctl.done(false);
					'''
				}
				State Post {
					entry '''
						timer.kill();
						testTimeCtl.disable();
						etUnit_closeTestCase(testId);
					'''
				}
			}
		}
	}

	Enumeration ETestEvent of int16 {
		POLLING_CYCLE = 0,
		TIMEOUT = 1,
		END = 2
	}

	DataClass DTestEvents {
		Attribute elements: ETestEvent ref
		Attribute size: int16
	}

	ProtocolClass PTestTimeControl {
		incoming {
			Message setTime(targetTime)
			Message enable()
			Message disable()
		}
		regular PortClass
		{
			usercode '''
				#include "base/etMemory.h"
				#include "debugging/etLogger.h"
				#include "debugging/etMSCLogger.h"
				#include "osal/etTime.h"
				#include "helpers/etTimeHelpers.h"
			'''
			Attribute self_time: targetTime
			Attribute isEnabled: boolean = "ET_FALSE"
			Operation getTestControlTime(t: targetTime ref) '''
				*t = self_time;
			'''
			Operation isTestControlEnabled(): boolean '''
				return isEnabled;
			'''
			handle incoming setTime '''
				self_time = *(etTime*)(((char*)msg)+MEM_CEIL(sizeof(etMessage)));
				etLogger_logInfoF("PTimeTestControl setTime(%d)", etTimeHelpers_convertToMSec(&self_time));
				(*receiveMessageFunc)(actor, self, msg);
			'''
			handle incoming enable '''
				isEnabled = ET_TRUE;
				(*receiveMessageFunc)(actor, self, msg);
			'''
			handle incoming disable '''
				isEnabled = ET_FALSE;
				(*receiveMessageFunc)(actor, self, msg);
			'''
		}
	}
}