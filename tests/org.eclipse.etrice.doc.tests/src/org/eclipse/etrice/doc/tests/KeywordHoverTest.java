/*******************************************************************************
 * Copyright (c) 2010 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.doc.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.etrice.core.common.ui.hover.IKeywordHoverContentProvider;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.common.collect.Sets;
import com.google.inject.Inject;

@RunWith(XtextRunner.class)
@InjectWith(RoomUiInjectorProvider.class)
public class KeywordHoverTest {

	private final static String[] existingKeywordsInHoverDoc = { "ActorClass", "TransitionPoint", "LogicalSystem",
			"SAP", "Attribute", "ActorRef", "StateMachine", "Port", "EntryPoint", "Operation", "AnnotationType", "SPP",
			"PrimitiveType", "attribute", "SubSystemRef", "Binding", "ProtocolClass", "RefinedState", "Enumeration",
			"ExternalType", "SubSystemClass", "LayerConnection", "DataClass", "ServiceImplementation", "State",
			"PortClass", "Transition", "ChoicePoint" };

	@Inject
	IKeywordHoverContentProvider contentProvider;

	@Inject
	IGrammarAccess grammar;

	/**
	 * Check that existing keyword hover doc is still present. Detect bug in lookup or deletion.
	 */
	@Test
	public void testKeywordsInHoverDoc() {
		Set<String> allKeywords = GrammarUtil.getAllKeywords(grammar.getGrammar());
		Set<String> availableKeywords = allKeywords.stream().filter(k -> contentProvider.getMdContent(k) != null)
				.collect(Collectors.toSet());
		Set<String> missingKeywords = Sets.difference(Sets.newHashSet(existingKeywordsInHoverDoc), availableKeywords);
		assertTrue("Keywords are missing in keyword hover doc (deleted?): " + missingKeywords, missingKeywords.isEmpty());
	}

	/**
	 * Simple check that contents are valid
	 */
	@Test
	public void testKeywordHoverContents() {
		Set<String> allKeywords = GrammarUtil.getAllKeywords(grammar.getGrammar());
		allKeywords.forEach(k -> {
			String content = contentProvider.getMdContent(k);
			if (content != null) {
				assertFalse(k + " should not be empty", content.isBlank());
				assertFalse(k + " should not contain heading", content.contains("##"));
			}
		});
		
		Arrays.asList(existingKeywordsInHoverDoc).forEach(k -> {
			String content = contentProvider.getMdContent(k);
			assertTrue(k + " should contain an example", content.contains("```room"));
		});
	}

	/**
	 * Ensure all keywords are present in documentation highlighting config
	 */
	@Test
	public void testKeywordsInPrism() throws IOException {
		String prismConfig = Files
				.readString(Paths.get("../../plugins/org.eclipse.etrice.doc/src/theme/prism-room.js"));
		Set<String> allKeywords = GrammarUtil.getAllKeywords(grammar.getGrammar());
		Set<String> presentKeywords = allKeywords.stream().filter(k -> prismConfig.contains(k))
				.collect(Collectors.toSet());
		// not configured
		presentKeywords.add("@");
		presentKeywords.add("ms");
		presentKeywords.add("target");
		Set<String> missingKeywords = Sets.difference(allKeywords, presentKeywords);
		assertTrue("Keywords are missing in highlighting config prism-room.js: " + missingKeywords,
				missingKeywords.isEmpty());
	}

}
