/*******************************************************************************
 * Copyright (c) 2011 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Henrik Rentz-Reichert (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.DataClass;
import org.eclipse.etrice.core.room.ProtocolClass;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.core.room.util.RoomHelpers;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CancelableDiagnostician;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.impl.ConcreteSyntaxEValidator;
import org.osgi.framework.Bundle;

import com.google.common.collect.Maps;

/**
 * Base class for tests helps with getting diagnostics from a model.
 *
 * @author Henrik Rentz-Reichert initial contribution and API
 *
 */
public class TestBase {

	private String basePath;
	
	protected RoomHelpers roomHelpers = new RoomHelpers();

	protected void prepare(Bundle bundle) {
		try {
			URL modelsDir = bundle.getEntry("models");
			URL fileURL = FileLocator.toFileURL(modelsDir);
			basePath = fileURL.getFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected Resource getResource(String modelName) {
		XtextResourceSet rs = new XtextResourceSet();
		rs.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		String path = basePath + modelName;
		URI uri = URI.createFileURI(path);
		return rs.getResource(uri, true);
	}
	
	public Diagnostic getDiag(EObject ele) {
		return getDiag(ele, true);
	}

	public Diagnostic getDiag(EObject ele, boolean disableConcreteSyntaxValidation) {
		return getDiag(ele, CheckMode.FAST_ONLY, true);
	}

	public Diagnostic getDiag(EObject ele, CheckMode mode, boolean disableConcreteSyntaxValidation) {
		Map<Object, Object> options = Maps.newHashMap();
		options.put(CheckMode.KEY, mode);
		options.put(CancelableDiagnostician.CANCEL_INDICATOR, CancelIndicator.NullImpl);
		
		if (disableConcreteSyntaxValidation) {
			// disable concrete syntax validation, since a semantic model that has been parsed 
			// from the concrete syntax always complies with it - otherwise there are parse errors.
			options.put(ConcreteSyntaxEValidator.DISABLE_CONCRETE_SYNTAX_EVALIDATOR, Boolean.TRUE);
		}
		// see EObjectValidator.getRootEValidator(Map<Object, Object>)
		options.put(EValidator.class, CoreTestsActivator.getInstance().getDiagnostician());
		return CoreTestsActivator.getInstance().getDiagnostician().validate(ele, options);
	}

	protected Stream<ActorClass> getActorClasses(RoomModel model) {
		return roomHelpers.getRoomClasses(model, ActorClass.class);
	}
	
	protected Stream<ProtocolClass> getProtocolClasses(RoomModel model) {
		return roomHelpers.getRoomClasses(model, ProtocolClass.class);
	}
	
	protected Stream<DataClass> getDataClasses(RoomModel model) {
		return roomHelpers.getRoomClasses(model, DataClass.class);
	}

	/**
	 * Helper predicate for matching against diagnostics, similar to
	 * o.e.xtext.testing.AssertableDiagnostics API/concept.
	 * 
	 * NOTE: AssertableDiagnostic is only available in xtext 2.26 or greater.
	 */
	public static class ContainsSubstringsPredicate implements Predicate<Diagnostic> {
		private final String[] substrings;
		private final int severity;

		public ContainsSubstringsPredicate(int severity, String... substrings) {
			this.severity = severity;
			this.substrings = substrings;
		}

		/**
		 * @param diag The Diagnostic instance to test against
		 * @return true if severity matches and all substrings are present in message, false otherwise
		 */
		@Override
		public boolean test(Diagnostic diag) {
			return (this.severity == diag.getSeverity()) && containsAll(substrings, diag.getMessage());
		}

		private boolean containsAll(String[] contains, String s) {
			List<String> substringList = Arrays.asList(substrings);
			return substringList.stream().allMatch(substring -> s.contains(substring));
		}

		@Override
		public String toString() {
			return "ContainsSubstrings (" + getSeverityString(severity) + ") " + Arrays.toString(substrings);
		}
	}

	/**
	 * Tests that the given Diagnostic matches the given predicate.
	 */
	public void assertSingleDiagMatch(Diagnostic diagnostic, Predicate<Diagnostic> predicate) {
		assertTrue(
			"Predicate:\n" + indent(predicate.toString()) + "\n" +
			"does not match the given diagnostic:\n" + indent(diagnostic.toString()),
			predicate.test(diagnostic)
		);
	}

	/**
	 * Test that in a given list of Diagnostics, at least one of them matches the given predicate.
	 * If the list of Diagnostics is empty, then the assert will fail.
	 */
	public void assertAnyDiagMatch(List<Diagnostic> diagnostics, Predicate<Diagnostic> predicate) {
		assertTrue(
			"Predicate:\n" + indent(predicate.toString()) + "\n" +
			"does not match any of the given diagnostics:\n" + indent(listToLines(diagnostics)),
			diagnostics.stream().anyMatch(predicate)
		);
	}

	/**
	 * Test that in a given list of Diagnostics, none of them match the given predicate.
	 * If the list of Diagnostics is empty, then the assert will pass.
	 */
	public void assertNoDiagMatch(List<Diagnostic> diagnostics, Predicate<Diagnostic> predicate) {
		List<Diagnostic> matches = diagnostics.stream()
				.filter(predicate)
				.collect(Collectors.toList());

		if (!matches.isEmpty()) {
			fail(
				"Predicate:\n" + indent(predicate.toString()) + "\n" +
				"unexpectedly matches the following diagnostics:\n" + indent(listToLines(matches))
			);
		}
	}

	private static String listToLines(List<? extends Object> elements) {
		return elements.stream()
				.map(elem -> elem.toString())
				.collect(Collectors.joining("\n"));
	}

	private static String indent(String s) {
		String[] lines = s.split("\n|\r\n|\r");
		return Arrays.asList(lines).stream()
				.map(line -> ("  " + line))
				.collect(Collectors.joining("\n"));
	}

	private static String getSeverityString(int severity) {
		StringBuilder result = new StringBuilder();
		result.append("0x").append(Integer.toHexString(severity)).append("=");
		switch (severity) {
		case Diagnostic.OK:
			result.append("OK");
			break;
		case Diagnostic.INFO:
			result.append("INFO");
			break;
		case Diagnostic.WARNING:
			result.append("WARNING");
			break;
		case Diagnostic.ERROR:
			result.append("ERROR");
			break;
		case Diagnostic.CANCEL:
			result.append("CANCEL");
			break;
		default:
			result.append("UNKNOWN");
			break;
		}
		return result.toString();
	}
}
