package org.eclipse.etrice.core

import java.util.Set
import org.eclipse.emf.common.util.Diagnostic
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.etrice.core.fsm.fSM.FSMPackage
import org.eclipse.etrice.core.fsm.fSM.InitialTransition
import org.eclipse.etrice.core.fsm.fSM.NonInitialTransition
import org.eclipse.etrice.core.fsm.fSM.StateGraph
import org.eclipse.etrice.core.room.ActorClass
import org.eclipse.etrice.core.room.RoomModel
import org.eclipse.xtend.lib.annotations.Data
import org.eclipse.xtext.validation.FeatureBasedDiagnostic
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class FSMValidatorTest extends TestBase {
	extension FSMPackage fsmPackage = FSMPackage.eINSTANCE

	@Before
	def void setUp() {
		prepare(CoreTestsActivator.getInstance().getBundle())
	}

	@Test
	def void transitionMissingSource() {
		val resource = getResource('validator/transitions_missing_source.room')
		val model = resource.contents.head as RoomModel
		val actorClass = model.roomClasses.filter(ActorClass).head
		val stateMachine = actorClass.stateMachine
		val diag = getDiag(model)
		assertSetEquals(
			newHashSet(
				"transition source not found" ->
					new DiagRef(stateMachine.transition("t2").from, stateTerminal_State, 0),
				"transition source not found" ->
					new DiagRef(stateMachine.transition("t4").from, subStateTrPointTerminal_TrPoint, 0),
				"transition source not found" ->
					new DiagRef(stateMachine.transition("t6").from, trPointTerminal_TrPoint, 0)
			),
			diag.featureBasedDiagnosticsSummary
		)
	}

	@Test
	def void transitionMissingTarget() {
		val resource = getResource('validator/transitions_missing_target.room')
		val model = resource.contents.head as RoomModel
		val actorClass = model.roomClasses.filter(ActorClass).head
		val stateMachine = actorClass.stateMachine
		val diag = getDiag(model)
		assertSetEquals(
			newHashSet(
				"transition target not found" -> new DiagRef(stateMachine.transition("t2").to, stateTerminal_State, 0),
				"transition target not found" ->
					new DiagRef(stateMachine.transition("t4").to, subStateTrPointTerminal_TrPoint, 0),
				"transition target not found" ->
					new DiagRef(stateMachine.subgraphOf("s2").transition("t21").to, trPointTerminal_TrPoint, 0)
			),
			diag.featureBasedDiagnosticsSummary
		)
	}

	@Test
	def void transitionInvalidEntryExitPoints() {
		val resource = getResource('validator/transitions_invalid_entry_exit.room')
		val model = resource.contents.head as RoomModel
		val actorClass = model.roomClasses.filter(ActorClass).head
		val stateMachine = actorClass.stateMachine
		val diag = getDiag(model)
		val withIgnored = newHashSet(
			"Unreachable state/point of graph" -> new DiagRef(stateMachine, stateGraph_TrPoints, 0),
			"Unreachable state/point of graph" -> new DiagRef(stateMachine, stateGraph_TrPoints, 1)
		)
		assertSetEquals(
			newHashSet(
				"entry point can not be transition target" ->
					new DiagRef(stateMachine.subgraphOf("s1").transition("t11").to, trPointTerminal_TrPoint, 0),
				"exit point can not be transition source" ->
					new DiagRef(stateMachine.subgraphOf("s2").transition("t21").from, trPointTerminal_TrPoint, 0),
				"entry and exit points forbidden on top level state graph" ->
					new DiagRef(stateMachine, stateGraph_TrPoints, 0),
				"entry and exit points forbidden on top level state graph" ->
					new DiagRef(stateMachine, stateGraph_TrPoints, 1),
				"sub state entry point can not be transition source" ->
					new DiagRef(stateMachine.transition("t2").from, subStateTrPointTerminal_TrPoint, 0),
				"sub state exit point can not be transition target" ->
					new DiagRef(stateMachine.transition("t3").to, subStateTrPointTerminal_TrPoint, 0)
			),
			diag.getFeatureBasedDiagnosticsSummary(withIgnored)
		)
	}

	@Data
	static class DiagRef {
		EObject source
		EStructuralFeature feature
		int index

		override toString() {
			feature.EContainingClass.name + "." + feature.name + "[" + index + "] of " +
				source.eResource.getURIFragment(source)
		}

	}

	// non-initial transitions
	def transition(StateGraph it, String nameToFind) {
		it.transitions.filter(NonInitialTransition).findFirst[name == nameToFind]
	}

	// initial transitions
	def initialTransition(StateGraph it, String nameToFind) {
		it.transitions.filter(InitialTransition).findFirst[name == nameToFind]
	}

	def state(StateGraph it, String nameToFind) {
		it.states.findFirst[name == nameToFind]
	}

	def subgraphOf(StateGraph it, String nameToFind) {
		it.state(nameToFind).subgraph
	}

	def getFeatureBasedDiagnosticsSummary(Diagnostic it) {
		getFeatureBasedDiagnosticsSummary(it, emptySet)
	}
	def getFeatureBasedDiagnosticsSummary(Diagnostic it, Set<Pair<String, DiagRef>> ignored) {
		children.filter(FeatureBasedDiagnostic).map[message -> new DiagRef(sourceEObject, feature, index)].reject [
			ignored.contains(it)
		].toSet
	}

	def <T> assertSetEquals(Set<? extends T> expected, Set<? extends T> actual) {
		val equals = expected.containsAll(actual) && actual.containsAll(expected)
		val missing = expected.reject[actual.contains(it)].toList
		val extra = actual.reject[expected.contains(it)].toList
		val message = "missing from actual:" + missing + ", extra in actual: " + extra
		Assert.assertTrue(message, equals)
	}
}
