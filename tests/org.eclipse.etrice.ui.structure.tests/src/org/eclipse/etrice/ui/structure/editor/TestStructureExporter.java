/*******************************************************************************
 * Copyright (c) 2022 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		epaen (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.structure.editor;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.ui.commands.RoomOpeningHelper;
import org.eclipse.etrice.ui.common.base.export.IBulkDiagramExporter;
import org.eclipse.etrice.ui.common.base.preferences.UIBasePreferenceConstants;
import org.eclipse.etrice.ui.structure.StructureTestActivator;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class TestStructureExporter {
	private final String DEFAULT_IMAGE_FORMAT = UIBasePreferenceConstants.FORMAT_JPG;
	private final String FOLDER_MODEL = "model";
	private final String FOLDER_GEN = "diagram-gen";

	IProject project;
	IFolder projectModelFolder;
	IFolder projectGenFolder;

	@Before
	public void setUp() throws CoreException, IOException {
		project = populateProject();
		projectModelFolder = project.getFolder(FOLDER_MODEL);
		projectGenFolder = project.getFolder(FOLDER_GEN);
	}

	@After
	public void tearDown() throws CoreException, IOException {
		removeProject(project);
	}

	/**
	 * Test method for {@link org.eclipse.etrice.ui.structure.editor.StructureExporter#export(org.eclipse.emf.ecore.EObject, java.lang.String)}.
	 */
	@Test
	public void testExport() {
		String exportPath = projectGenFolder.getLocation().toOSString();
		String expectedFilename = "TestStructureExporter.ac_test_structure." + DEFAULT_IMAGE_FORMAT;
		RoomModel model = loadModel(getModelFileName());
		ActorClass ac = getActorClass(model, "ac_test");
		IBulkDiagramExporter exporter = RoomOpeningHelper.getStructureDiagramAccess().getDiagramExporter();
		exporter.export(ac, exportPath);
		try {
			projectGenFolder.refreshLocal(IResource.DEPTH_ONE, null);
		} catch (Exception e) {
			fail("gen folder refresh failed");
		}
		assertTrue(projectGenFolder.getFile(expectedFilename).exists());
	}

	@Test
	public void testExportDiffNamespace() {
		String exportPath = projectGenFolder.getLocation().toOSString();
		String[] expectedFilenames = {
				"TestStructureExporter.ac_test_structure." + DEFAULT_IMAGE_FORMAT,
				"TestStructureExporterDiffNamespace.ac_test_structure." + DEFAULT_IMAGE_FORMAT
		};
		ActorClass ac = getActorClass("TestStructureExporter.room", "ac_test");
		ActorClass ac2 = getActorClass("TestStructureExporterDiffNamespace.room", "ac_test");
		IBulkDiagramExporter exporter = RoomOpeningHelper.getStructureDiagramAccess().getDiagramExporter();
		exporter.export(ac, exportPath);
		exporter.export(ac2, exportPath);
		try {
			projectGenFolder.refreshLocal(IResource.DEPTH_ONE, null);
		} catch (Exception e) {
			fail("gen folder refresh failed");
		}
		for (String filename : expectedFilenames) {
			assertTrue(projectGenFolder.getFile(filename).exists());
		}
	}

	protected String getModelFileName() {
		return "TestStructureExporter.room";
	}
	
	protected URL getModelsDirectory() {
		return StructureTestActivator.getInstance().getBundle().getEntry("models");
	}

	protected IProject populateProject() throws CoreException, IOException {
		URL fileURL = FileLocator.toFileURL(getModelsDirectory());
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("TestProject");
		project.create(null);
		project.open(null);

		// link to external model source folder and copy as internal model folder
		{
			IFolder folder = project.getFolder("model_src");
			IPath modelSrcPath = new Path(fileURL.getPath());
			folder.createLink(modelSrcPath, 0, null);
			folder.copy(new Path(FOLDER_MODEL), 0, null);
		}

		// create output gen folder
		{
			IFolder folder = project.getFolder(FOLDER_GEN);
			folder.create(0, true, null);
		}

		return project;
	}

	protected void removeProject(IProject project) throws CoreException, IOException {
		project.delete(0, null);
	}

	private RoomModel loadModel(String modelName) {
		XtextResourceSet rs = new XtextResourceSet();
		rs.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		IFile modelFile = projectModelFolder.getFile(modelName);
		URI uri = URI.createFileURI(modelFile.getLocation().toFile().getAbsolutePath());
		Resource resource = rs.getResource(uri, true);
		Object root = resource.getContents().get(0);
		if (!(root instanceof RoomModel)) {
			fail();
		}
		return (RoomModel)root;
	}
	
	protected ActorClass getActorClass(RoomModel model, String actorName) {
		EObject obj = model.eResource().getEObject("ActorClass:"+actorName);
		if (!(obj instanceof ActorClass)) {
			fail();
		}
		return (ActorClass)obj;
	}
	
	protected ActorClass getActorClass(String modelName, String actorName) {
		RoomModel model = loadModel(modelName);
		ActorClass ac = getActorClass(model, actorName);
		return ac;
	}
}
