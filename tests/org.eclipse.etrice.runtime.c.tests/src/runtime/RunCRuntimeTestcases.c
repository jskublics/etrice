/*******************************************************************************
 * Copyright (c) 2011 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * CONTRIBUTORS:
 * 		Thomas Schuetz (initial contribution)
 *
 *******************************************************************************/


/*
 * RunRuntimeTestcases.c
 *
 *  Created on: 12.01.2012
 *      Author: tschuetz
 */


#include "RunCRuntimeTestcases.h"

#include "TestEtMessage.h"
#include "TestEtMessageQueue.h"
#include "TestEtMessageService.h"
#include "TestEtUnit.h"
#include "TestEtUnitNegative.h"
#include "TestEtMemory.h"
#include "TestEtQueue.h"
#include "TestEtTimer.h"
#include "TestEtDatatypes.h"
#include "TestEtConsoleLogger.h"
#include "helpers/TestEtTimeHelpers.h"
#include "util/TestUtil.h"

#include "container/TestEtStaticDeque.h"

#include "etUnit/etUnit.h"
#include "debugging/etMSCLogger.h"
#include "debugging/etLogger.h"

void RunCRuntimeTestcases(void){
	// test etUnit
	etUnit_open("log","TestEtUnit");
	TestEtUnit_runSuite();
	etUnit_close();

	// Negative tests for etUnit. All test cases in this suite are expected to fail.
	etUnit_open("log","TestEtUnitNegative");
	TestEtUnitNegative_runSuite();
	etUnit_close();

	/* special situation for testing openAll and closeAll of etUnit
	 * this has to be done outside of etUnit_open and etUnit_close */
	int id = etUnit_openAll("log","TestEtUnitSpecial", "org.eclipse.etrice.runtime.c.tests.etUnit", "openAll_and_closeAll");
	ETUNIT_EQUAL_INT("Open and Close", 0, id);
	etUnit_closeAll(id);

	etMSCLogger_open("log", "test.log");
	etUnit_open("log","TestCRuntime");
	
	TestEtQueue_runSuite();
	TestEtMemory_runSuite();
	TestEtMessage_runSuite();
	TestEtMessageQueue_runSuite();
	TestEtMessageService_runSuite();
	TestEtTimer_runSuite();
	TestEtDatatypes_runSuite();
	TestEtTimeHelpers_runSuite();
	TestUtil_runSuite();
	TestEtConsoleLogger_runSuite();
	TestEtStaticDeque_runSuite();

	etUnit_close();
	etMSCLogger_close();
}

