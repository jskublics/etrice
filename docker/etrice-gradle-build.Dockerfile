FROM eclipse-temurin:17-jdk-noble AS gradle-build
# install gcc for generator tests
RUN apt update && apt install -y build-essential && apt clean
# configure a gradle cache accessible to all users
RUN mkdir --mode a=u /gradle /gradle/caches /gradle/caches/modules-2 /gradle/wrapper /gradle/wrapper/dists
ENV GRADLE_USER_HOME=/gradle

FROM gradle-build AS gradle-cache
ARG ETRICE_GIT_REF=master
RUN wget https://gitlab.eclipse.org/eclipse/etrice/etrice/-/archive/$ETRICE_GIT_REF/etrice-$ETRICE_GIT_REF.tar.gz \
    && tar -xf etrice-$ETRICE_GIT_REF.tar.gz
WORKDIR ./etrice-$ETRICE_GIT_REF
# run gradle build to populate the gradle cache
RUN ./gradlew --no-daemon build
# remove lock files files from cache, see https://docs.gradle.org/current/userguide/dependency_resolution.html#sub:ephemeral-ci-cache
RUN rm -f /gradle/caches/modules-2/*.lock /gradle/caches/modules-2/gc.properties /gradle/wrapper/dists/*/*/*.zip /gradle/wrapper/dists/*/*/*.zip.lck 
RUN chmod -R a=u /gradle/caches/modules-2 /gradle/wrapper/dists

FROM gradle-build AS gradle-build-with-cache
COPY --from=gradle-cache /gradle/caches/modules-2 /gradle/caches/modules-2/
COPY --from=gradle-cache /gradle/wrapper/dists /gradle/wrapper/dists/
ENTRYPOINT /bin/bash
