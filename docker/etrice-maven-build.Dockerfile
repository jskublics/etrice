# jre is sufficient because tycho compiles with eclipse jdt
FROM eclipse-temurin:17-jre-noble AS maven-build
# install mutter for ui tests
RUN apt update && apt install -y libswt-gtk-4-java libswt-gtk-4-jni dbus-x11 mutter && apt clean
# XDG_RUNTIME_DIR is required by mutter
RUN mkdir --mode a=u /tmp/runtime-root
ENV XDG_RUNTIME_DIR=/tmp/runtime-root
# configure a maven cache accessible to all users
RUN mkdir --mode a=u /m2 /m2/repository
ENV MAVEN_ARGS="-Dmaven.repo.local=/m2/repository"

FROM maven-build AS maven-cache
ARG ETRICE_GIT_REF=master
RUN wget https://gitlab.eclipse.org/eclipse/etrice/etrice/-/archive/$ETRICE_GIT_REF/etrice-$ETRICE_GIT_REF.tar.gz \
    && tar -xf etrice-$ETRICE_GIT_REF.tar.gz
WORKDIR ./etrice-$ETRICE_GIT_REF
# run maven tycho build to populate the maven cache
RUN eval $(dbus-launch --sh-syntax) \
    && mutter --headless --sm-disable --no-x11 --virtual-monitor 1280x720 & \
    ./mvnw tycho-versions:set-version -DnewVersion=0.0.0-SNAPSHOT -Dtycho.mode=maven \
    && ./mvnw verify
RUN chmod -R a=u /m2/repository

FROM maven-build AS maven-build-with-cache
COPY --from=maven-cache /m2/repository /m2/repository/
ENTRYPOINT /bin/bash
